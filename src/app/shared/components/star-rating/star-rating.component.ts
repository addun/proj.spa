import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-star-rating',
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.css']
})
export class StarRatingComponent implements OnInit {
  @Output()rateChanged = new EventEmitter();
  @Input()editMode = false;
  @Input()size = 1;
  @Input()rate;

  actualRate = 1;

  constructor() { }

  ngOnInit() {
    this.setActualRate(this.rate);

  }

  counter(i: number) {
    return new Array(i);
  }

  private setActualRate(number: number) {
    if (number === null) {
      return;
    }
    if (number < 1 && number > 5 ) {
      return;
    }
    this.actualRate = number;
  }
}
