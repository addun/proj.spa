import {Component, Input, OnInit} from '@angular/core';
import {IEmployerJobOffer} from '../../../interfaces/IEmployerJobOffer';
import {Router} from '@angular/router';

@Component({
  selector: 'app-employee-job-offer-tab',
  templateUrl: './employee-job-offer-tab.component.html',
  styleUrls: ['./employee-job-offer-tab.component.css']
})
export class EmployeeJobOfferTabComponent implements OnInit {
  @Input() jobOffer: IEmployerJobOffer;

  constructor(private router: Router) { }

  ngOnInit() {
    console.log(this.jobOffer);
  }

  jobOfferDetails(offerId: string) {
    this.router.navigate(['/job-offer/' + offerId], { queryParams: { searchurl: this.router.url } });
  }
}
