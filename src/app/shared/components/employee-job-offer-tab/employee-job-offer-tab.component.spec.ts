import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeJobOfferTabComponent } from './employee-job-offer-tab.component';

describe('EmployeeJobOfferTabComponent', () => {
  let component: EmployeeJobOfferTabComponent;
  let fixture: ComponentFixture<EmployeeJobOfferTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeJobOfferTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeJobOfferTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
