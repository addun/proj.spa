import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-operation-success',
  templateUrl: './operation-success.component.html',
  styleUrls: ['./operation-success.component.css']
})
export class OperationSuccessComponent implements OnInit {
  @Input()title: string;
  @Input()secondary: string;

  constructor() { }

  ngOnInit() {
  }
}
