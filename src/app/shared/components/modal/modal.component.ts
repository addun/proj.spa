import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-employee-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {
  @Input() message;
  @Input() dismissButtonTitle = 'Anuluj';
  @Input() closeButtonTitle = 'Zamknij';

  constructor(public activeModal: NgbActiveModal) {}

  ngOnInit(): void {
  }
}
