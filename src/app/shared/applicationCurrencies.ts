interface ICurrency {
  PLN: string;
  EUR: string;
  USD: string;
  GBP: string;
}

class Currency implements ICurrency {
  EUR = 'EUR';
  GBP = 'GBP';
  PLN = 'PLN';
  USD = 'USD';
}

export const applicationCurrencies = <ICurrency>new Currency();
