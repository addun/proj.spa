interface IGender {
  man: string;
  woman: string;
  nodefined: string;
}

interface IGenderTranslator {
  translateGender(propertyName: string): string;
  toGenderValue(value: string): string;
}

class ApplicationGenders implements IGender {
  man = 'man';
  woman = 'woman';
  nodefined = 'nodefined';
}

class PolishGenderTranslator implements IGenderTranslator {
  man = 'mężczyzna';
  woman = 'kobieta';
  nodefined = 'wolę nie podawać';
  translateGender(propertyName: string): string {
    const result = this[propertyName];
    return result ? result : null;
  }
  toGenderValue(value: string): string {
    let result = null;
    for (const property in this) {
      if (this.hasOwnProperty(property)) {
        if (value === this[property].toString()) {
          result = property;
        }
      }
    }
    console.log(result);
    return result;
  }
}

export const applicationGenderOptions = new ApplicationGenders();
export const polishApplicationGenderOptionsTranslator = <IGenderTranslator>new PolishGenderTranslator();

