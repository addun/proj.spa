interface IJobExperience {
  JUNIOR: string;
  MID: string;
  SENIOR: string;
  ANY: string;
}

class JobExperience implements IJobExperience {
  ANY = 'ANY';
  JUNIOR = 'JUNIOR';
  MID = 'MID';
  SENIOR = 'SENIOR';

}

export const applicationJobExperience = <IJobExperience>new JobExperience();
