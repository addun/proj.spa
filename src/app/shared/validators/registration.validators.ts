import { AbstractControl, ValidatorFn } from '@angular/forms';

export class RegistrationValidators {
    static ConfirmEqualValidator(controlNameToCompare: string): ValidatorFn {
        return (control: AbstractControl): {[key: string]: any} | null => {
            if (control.value === null || control.value.length === 0) {
                return null;
            }

            const controlToCompare = control.root.get(controlNameToCompare);
            if (controlToCompare) {
                const subscription = controlToCompare.valueChanges.subscribe(() => {
                    control.updateValueAndValidity();
                    subscription.unsubscribe();
                });
            }
            return controlToCompare.value !== control.value ? {'passwordNotMatch': true} : null;
        };
    }

    static ConfirmNipFormat(control: AbstractControl): {[key: string]: any} | null {
        if (control.value === null || control.value.length === 0) {
            return null;
        }

        if (isNaN(Number(control.value))) {
            return {'nipFormatError': { message: 'Format jest nieprawidłowy (Wartość nie może zawierać liter)'}};
        } else if (control.value.length < 10) {
            return {'nipFormatError': { message: 'Format jest nieprawidłowy (Minimalna długość: 10)'}};
        } else if (control.value.length > 11) {
            return {'nipFormatError': { message: 'Format jest nieprawidłowy (Maksymalna długość: 11)'}};
        }
        return null;
    }
}

