import { NgModule } from '@angular/core';
import { CapitalizePipe } from './pipes/capitalize.pipe';
import { CommonModule } from '@angular/common';
import { OperationSuccessComponent } from './components/operation-success/operation-success.component';
import { ErrorComponent } from './components/error/error.component';
import { RouterModule } from '@angular/router';
import { StarRatingComponent } from './components/star-rating/star-rating.component';
import {ReactiveFormsModule} from '@angular/forms';
import {ModalComponent} from './components/modal/modal.component';
import {NgbModalModule} from '@ng-bootstrap/ng-bootstrap';
import {SafePipe} from './pipes/safe.pipe';
import {JobOfferTabComponent} from '../employer/employer-job-offers/job-offer-tab/job-offer-tab.component';
import {EmployeeJobOfferTabComponent} from './components/employee-job-offer-tab/employee-job-offer-tab.component';

@NgModule({
  declarations: [
    CapitalizePipe,
    OperationSuccessComponent,
    ErrorComponent,
    StarRatingComponent,
    ModalComponent,
    SafePipe,
    EmployeeJobOfferTabComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    NgbModalModule,
  ],
  exports: [
    CapitalizePipe,
    OperationSuccessComponent,
    StarRatingComponent,
    SafePipe,
    EmployeeJobOfferTabComponent
  ],
})
export class SharedModule { }
