import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, switchMap } from 'rxjs/internal/operators';
import {AccountService} from '../../account/account.service';
import {Router} from '@angular/router';

@Injectable()
export class AuthErrorInterceptor implements HttpInterceptor {

  constructor(private accountService: AccountService, private router: Router) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request)
      .pipe(catchError(err => {
      if (err.status === 401 && err.headers.has('token-expired')) {
        return this.accountService.refreshToken()
          .pipe(switchMap(() => {
            request = request.clone({
              setHeaders: {
                Authorization: `Bearer ${this.accountService.getToken()}`
              }
            });
            return next.handle(request);
          }));
      } else if (err.status === 401 || err.status === 403) {
        this.accountService.logout();
        this.router.navigate(['/login']);
        return throwError(err);
      } else {
        return throwError(err);
      }
    }));
  }
}
