import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable()
export class AutocompleteService {
  private baseUrl = 'http://localhost:5000/api/';
  //private baseUrl = 'https://justfindit-api.azurewebsites.net/api/';

  constructor(private httpClient: HttpClient) {}

  autocompleteSkills(phrase: string) {
    console.log(this.baseUrl + 'autocomplete/skills?phrase=' + phrase);
    return this.httpClient.get(this.baseUrl + 'autocomplete/skills?phrase=' + phrase);
  }

  autocompleteLanguages(phrase: string) {
    console.log(this.baseUrl + 'autocomplete/languages?phrase=' + phrase);
    return this.httpClient.get(this.baseUrl + 'autocomplete/languages?phrase=' + phrase);
  }
}
