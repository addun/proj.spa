import {Component, OnDestroy, OnInit} from '@angular/core';
import {EmployerService} from '../employer.service';
import {Subscription} from 'rxjs';
import {IEmployer} from '../../interfaces/IEmployer';
import {IEmployerLocation} from '../../interfaces/IEmployerLocation';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-employer-public-profile',
  templateUrl: './employer-public-profile.component.html',
  styleUrls: ['./employer-public-profile.component.css']
})
export class EmployerPublicProfileComponent implements OnInit, OnDestroy {
  aboutBaseShortenCharLimit = 600;
  aboutShortenCharLimit = 600;
  hasEmailConfirmed = true;
  employerData: IEmployer;
  headquarters: IEmployerLocation;

  employerProfileSubscription: Subscription;

  constructor(private employerService: EmployerService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    const id = this.activatedRoute.snapshot.params['id'];
    this.employerProfileSubscription = this.employerService.getEmployerProfileById(id)
      .subscribe((employer: IEmployer) => {
        console.log(employer);
        this.employerData = employer;
        this.headquarters = employer.locations
          .find(location => location.employerLocationId === employer.headquartersId);
        this.hasEmailConfirmed = employer.emailConfirmed;
    });
  }

  isAboutCharLimitReached(): boolean {
    return this.employerData.companyDescription.length > this.aboutBaseShortenCharLimit;
  }

  isInShowMoreMode(): boolean {
    return this.aboutShortenCharLimit !== this.aboutBaseShortenCharLimit;
  }

  showLess() {
    this.aboutShortenCharLimit = this.aboutBaseShortenCharLimit;
  }

  showMore() {
    this.aboutShortenCharLimit = this.employerData.companyDescription.length;
  }

  ngOnDestroy(): void {
    this.employerProfileSubscription.unsubscribe();
  }
}
