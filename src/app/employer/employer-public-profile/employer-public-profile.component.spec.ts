import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployerPublicProfileComponent } from './employer-public-profile.component';

describe('EmployerPublicProfileComponent', () => {
  let component: EmployerPublicProfileComponent;
  let fixture: ComponentFixture<EmployerPublicProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployerPublicProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployerPublicProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
