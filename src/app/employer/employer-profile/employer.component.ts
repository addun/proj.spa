import {Component, OnDestroy, OnInit} from '@angular/core';
import { IEmployer } from '../../interfaces/IEmployer';
import {EmployerService} from '../employer.service';
import {Subscription} from 'rxjs';
import {FormControl, FormGroup} from '@angular/forms';
import {ModalComponent} from '../../shared/components/modal/modal.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {IEmployerLocation} from '../../interfaces/IEmployerLocation';

@Component({
  selector: 'app-employer',
  templateUrl: './employer.component.html',
  styleUrls: ['./employer.component.css']
})
export class EmployerComponent implements OnInit, OnDestroy {
  aboutBaseShortenCharLimit = 600;
  aboutShortenCharLimit = 600;
  hasEmailConfirmed = true;
  employerData: IEmployer;
  headquarters: IEmployerLocation;

  whatsWeCreatingEditMode = false;
  changingWhatsWeCreatingDescriptionInProgress = false;
  whatsWeCreatingDescriptionForm: FormGroup;

  workingWithUsDescriptionEditMode = false;
  changingWorkingWithUsDescriptionInProgress = false;
  workingWithUsDescriptionForm: FormGroup;


  internshipDescriptionEditMode = false;
  changingInternshipDescriptionInProgress = false;
  internshipDescriptionForm: FormGroup;


  currentEmployerSubscription: Subscription;

  tryDeleteBenefitSubscription: Subscription;
  tryDeleteLocationSubscription: Subscription;
  tryDeleteTechnologySubscription: Subscription;

  constructor(private modalService: NgbModal, private employerService: EmployerService) { }

  ngOnInit() {
    this.currentEmployerSubscription = this.employerService.getCurrentEmployer().subscribe(employer => {
      console.log(employer);
      this.employerData = employer;
      this.headquarters = employer.locations
        .find(location => location.employerLocationId === employer.headquartersId);
      this.hasEmailConfirmed = employer.emailConfirmed;
    });

    // MODAL DELETE CONFIRMS
    this.tryDeleteLocationSubscription
      = this.employerService.tryDeleteEmployerLocation$.subscribe((locationId) => {
      const modalRef = this.modalService.open(ModalComponent);
      modalRef.componentInstance.message = 'Czy na pewno chcesz usunąć lokację?';
      modalRef.componentInstance.closeButtonTitle = 'Usuń';
      modalRef.componentInstance.dismissButtonTitle = 'Anuluj';

      modalRef.result.then(() => {
        this.employerService.confirmDeletionEmployerLocationElement(locationId);
      }, () => {});
    });

    this.tryDeleteBenefitSubscription
      = this.employerService.tryDeleteBenefitElement$.subscribe((benefitId) => {
      const modalRef = this.modalService.open(ModalComponent);
      modalRef.componentInstance.message = 'Czy na pewno chcesz usunąć benefit?';
      modalRef.componentInstance.closeButtonTitle = 'Usuń';
      modalRef.componentInstance.dismissButtonTitle = 'Anuluj';

      modalRef.result.then(() => {
        this.employerService.confirmDeletionBenefitElement(benefitId);
      }, () => {});
    });

    this.tryDeleteTechnologySubscription
      = this.employerService.tryDeleteEmployerTechnology$.subscribe((technologyId) => {
      const modalRef = this.modalService.open(ModalComponent);
      modalRef.componentInstance.message = 'Czy na pewno chcesz usunąć technologię?';
      modalRef.componentInstance.closeButtonTitle = 'Usuń';
      modalRef.componentInstance.dismissButtonTitle = 'Anuluj';

      modalRef.result.then(() => {
        this.employerService.confirmDeletionEmployerTechnologyElement(technologyId);
      }, () => {});
    });
  }

  cancelEditInternshipDescription() {
    this.internshipDescriptionEditMode = false;
    this.resetInternshipDescriptionForm();
  }

  cancelEditWhatsWeCreatingDescription() {
    this.whatsWeCreatingEditMode = false;
    this.resetWhatsWeCreatingDescriptionForm();
  }

  cancelEditWorkingWorkWithUsDescription() {
    this.workingWithUsDescriptionEditMode = false;
    this.resetWorkingWithUsDescriptionForm();
  }

  editInternshipDescription() {
    this.internshipDescriptionEditMode = true;
    this.initializeInternshipDescriptionForm();
  }

  editWhatsWeCreatingDescription() {
    this.whatsWeCreatingEditMode = true;
    this.initializeWhatsWeCreatingDescriptionForm();
  }

  editWorkingWithUsDescription() {
    this.workingWithUsDescriptionEditMode = true;
    this.initializeWorkingWithUsDescriptionForm();
  }

  isAboutCharLimitReached(): boolean {
    return this.employerData.companyDescription.length > this.aboutBaseShortenCharLimit;
  }

  isInShowMoreMode(): boolean {
    return this.aboutShortenCharLimit !== this.aboutBaseShortenCharLimit;
  }

  showLess() {
    this.aboutShortenCharLimit = this.aboutBaseShortenCharLimit;
  }

  showMore() {
    this.aboutShortenCharLimit = this.employerData.companyDescription.length;
  }

  submitInternshipDescription() {
    this.changingInternshipDescriptionInProgress = true;
    const description = this.internshipDescriptionForm.get('internshipDescription').value;

    this.employerService.editInternshipDescription(description)
      .subscribe(() => {
          this.cancelEditInternshipDescription();
        },
        error => console.log(error))
      .add(() => this.changingInternshipDescriptionInProgress = false);
  }

  submitWhatsWeCreatingDescription() {
    this.changingWhatsWeCreatingDescriptionInProgress = true;
    const description = this.whatsWeCreatingDescriptionForm.get('whatsWeCreating').value;

    this.employerService.editWhatsWeCreatingDescription(description)
      .subscribe(() => {
          this.cancelEditWhatsWeCreatingDescription();
        },
        error => console.log(error))
      .add(() => this.changingWhatsWeCreatingDescriptionInProgress = false);
  }

  submitWorkingWithUsDescription() {
    this.changingWorkingWithUsDescriptionInProgress = true;
    const description = this.workingWithUsDescriptionForm.get('workingWithUs').value;

    this.employerService.editWorkingWithUsDescription(description)
      .subscribe(() => {
          this.cancelEditWorkingWorkWithUsDescription();
        },
        error => console.log(error))
      .add(() => this.changingWorkingWithUsDescriptionInProgress = false);
  }

  ngOnDestroy(): void {
    this.currentEmployerSubscription.unsubscribe();
    this.tryDeleteBenefitSubscription.unsubscribe();
    this.tryDeleteLocationSubscription.unsubscribe();
    this.tryDeleteTechnologySubscription.unsubscribe();
  }

  private initializeInternshipDescriptionForm() {
    this.internshipDescriptionForm = new FormGroup({
      'internshipDescription': new FormControl(this.employerData.internshipDescription),
    });
  }

  private initializeWhatsWeCreatingDescriptionForm() {
    this.whatsWeCreatingDescriptionForm = new FormGroup({
      'whatsWeCreating': new FormControl(this.employerData.whatsWeCreatingDescription),
    });
  }

  private initializeWorkingWithUsDescriptionForm() {
    this.workingWithUsDescriptionForm = new FormGroup({
      'workingWithUs': new FormControl(this.employerData.workingWithUsDescription),
    });
  }

  private resetInternshipDescriptionForm() {
    this.internshipDescriptionForm.reset();
  }

  private resetWhatsWeCreatingDescriptionForm() {
    this.whatsWeCreatingDescriptionForm.reset();
  }

  private resetWorkingWithUsDescriptionForm() {
    this.workingWithUsDescriptionForm.reset();
  }
}
