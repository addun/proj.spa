import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyBenefitsComponent } from './company-benefits.component';

describe('CompanyBenefitsComponent', () => {
  let component: CompanyBenefitsComponent;
  let fixture: ComponentFixture<CompanyBenefitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyBenefitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyBenefitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
