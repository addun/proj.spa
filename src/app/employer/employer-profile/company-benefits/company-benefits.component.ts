import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import {EmployerService} from '../../employer.service';
import {IEmployerBenefit} from '../../../interfaces/IEmployerBenefit';

@Component({
  selector: 'app-company-benefits',
  templateUrl: './company-benefits.component.html',
  styleUrls: ['./company-benefits.component.css']
})
export class CompanyBenefitsComponent implements OnInit, OnDestroy {
  employerBenefitAddMode = false;
  employerBenefitEditMode = false;
  addingOrEditingEmployerBenefitInProgress = false;
  deletingEmployerBenefitInProgress = false;
  currentEditedEmployerBenefitElementIndex = -1;
  currentDeletedEmployerBenefitElementIndex = -1;

  employerBenefits: Array<{ isInEditMode: boolean, companyBenefit: IEmployerBenefit }>;
  employerBenefitForm: FormGroup;
  employerBenefit: IEmployerBenefit;

  // SUBSCRIPTIONS
  currentEmployerSubscription: Subscription;
  confirmDeletionEmployerBenefitSubscription: Subscription;

  constructor(private employerService: EmployerService) { }

  ngOnInit() {
    this.currentEmployerSubscription = this.employerService.getCurrentEmployer().subscribe(employer => {
      this.employerBenefits = [];
      for (let i = 0; i < employer.benefits.length; i++) {
        this.employerBenefits.push({isInEditMode: false, companyBenefit: employer.benefits[i]});
      }
    });

    this.confirmDeletionEmployerBenefitSubscription
      = this.employerService.confirmDeletionBenefitElement$.subscribe((benefitElementId) => {
      this.deletingEmployerBenefitInProgress = true;
      this.employerService.deleteBenefitElement(benefitElementId)
        .subscribe().add(() => {
          this.deletingEmployerBenefitInProgress = false;
          this.currentDeletedEmployerBenefitElementIndex = -1;
        });
      console.log('delete ' + benefitElementId);
    });
  }

  submitChanges() {
    this.addingOrEditingEmployerBenefitInProgress = true;
    this.assignFormValues();
    if (this.employerBenefitAddMode) {
      this.employerService.addEmployerBenefit(this.employerBenefit)
        .subscribe(() => {
            this.cancelAddingOrEditingEmployerBenefit();
          },
          error => console.log(error))
        .add(() => this.addingOrEditingEmployerBenefitInProgress = false);
    } else {
      this.employerService.editEmployerBenefit(this.employerBenefit)
        .subscribe(() => {
          this.cancelAddingOrEditingEmployerBenefit();
        })
        .add(() => this.addingOrEditingEmployerBenefitInProgress = false);
    }
  }

  addEmployerBenefit() {
    this.employerBenefitEditMode = false;
    this.employerBenefitAddMode = true;
    this.initializeNewForm();
  }

  cancelAddingOrEditingEmployerBenefit() {
    this.employerBenefitAddMode = false;
    this.employerBenefitEditMode = false;
    this.currentEditedEmployerBenefitElementIndex = -1;
    this.resetForm();
  }

  deleteEmployerBenefitElement(elementIndex: number) {
    this.currentDeletedEmployerBenefitElementIndex = elementIndex;
    this.employerService.tryDeleteBenefitElement(this.employerBenefits[elementIndex].companyBenefit.benefitId);
  }

  editEmployerBenefitElement(elementIndex: number) {
    this.employerBenefitAddMode = false;
    this.currentEditedEmployerBenefitElementIndex = elementIndex;
    this.employerBenefitEditMode = true;
    this.initializeEditForm(elementIndex);
  }

  isEmployerBenefitElementEdited(elementIndex: number) {
    return this.currentEditedEmployerBenefitElementIndex === elementIndex;
  }

  ngOnDestroy(): void {
    this.currentEmployerSubscription.unsubscribe();
    this.confirmDeletionEmployerBenefitSubscription.unsubscribe();
  }

  // Private
  private assignFormValues() {
    this.employerBenefit = {
      benefitId: this.employerBenefitEditMode
        ? this.employerBenefits[this.currentEditedEmployerBenefitElementIndex].companyBenefit.benefitId
        : null,
      title: this.employerBenefitForm.get('title').value,
      description: this.employerBenefitForm.get('description').value,
    };
  }

  private initializeNewForm() {
    this.employerBenefitForm = new FormGroup({
      'title': new FormControl(null, Validators.required),
      'description': new FormControl(null),
    });
  }

  private initializeEditForm(elementIndex: number) {
    this.employerBenefitForm = new FormGroup({
      'title': new FormControl(this.employerBenefits[elementIndex].companyBenefit.title, Validators.required),
      'description': new FormControl(this.employerBenefits[elementIndex].companyBenefit.description),
    });
  }

  private resetForm() {
    this.employerBenefitForm.reset();
  }
}
