import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {IEmployeePersonalData} from '../../../interfaces/IEmployeePersonalData';
import {IEmployer} from '../../../interfaces/IEmployer';
import {EmployerService} from '../../employer.service';
import {RegistrationValidators} from '../../../shared/validators/registration.validators';
import {IEmployerData} from '../../../interfaces/IEmployerData';

@Component({
  selector: 'app-company-data',
  templateUrl: './company-data.component.html',
  styleUrls: ['./company-data.component.css']
})
export class CompanyDataComponent implements OnInit {
  employerData: IEmployer;
  employerCompanyData: IEmployerData;
  companyDataEditMode = false;
  editCompanyDataForm: FormGroup;
  changingCompanyDataInProgress = false;

  constructor(private employerService: EmployerService) { }

  ngOnInit() {
    this.employerService.getCurrentEmployer().subscribe(employer => this.employerData = employer);
  }

  cancelEditCompanyData() {
    this.companyDataEditMode = false;
    this.resetForm();
  }

  editCompanyData() {
    this.companyDataEditMode = true;
    this.initializeForm();
  }

  submitCompanyData() {
    this.changingCompanyDataInProgress = true;
    this.assignFormValues();
    this.employerService.editEmployerData(this.employerCompanyData)
      .subscribe(() => this.companyDataEditMode = false,
        (errorResponse) => {
          const errors = errorResponse.error;
          for (let key in errors) {
            this.editCompanyDataForm.get(key).setErrors({'serverError' : errors[key]});
          }
        })
      .add(() => this.changingCompanyDataInProgress =  false);
  }

  private assignFormValues() {
    this.employerCompanyData = {
      companyName: this.editCompanyDataForm.get('companyName').value,
      email: this.editCompanyDataForm.get('email').value,
      companyDescription: this.editCompanyDataForm.get('companyDescription').value,
      foundedYear: this.editCompanyDataForm.get('foundedYear').value,
      companySize: this.editCompanyDataForm.get('companySize').value,
      nip: this.editCompanyDataForm.get('nip').value,
      websiteUrl: this.editCompanyDataForm.get('websiteUrl').value,
    };
  }

  private initializeForm() {
    this.editCompanyDataForm = new FormGroup({
      'companyName': new FormControl(this.employerData.companyName, Validators.required),
      'companyDescription': new FormControl(this.employerData.companyDescription),
      'email': new FormControl(this.employerData.email, [Validators.required, Validators.email]),
      'foundedYear': new FormControl(this.employerData.foundedYear),
      'companySize': new FormControl(this.employerData.companySize, Validators.required),
      'nip': new FormControl(this.employerData.nip, [Validators.required, RegistrationValidators.ConfirmNipFormat]),
      'websiteUrl': new FormControl(this.employerData.websiteUrl, Validators.required),
    });
  }

  private resetForm() {
    this.editCompanyDataForm.reset();
  }

}
