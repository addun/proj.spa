import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {EmployerService} from '../../employer.service';
import {Subscription} from 'rxjs';
import {IEmployerLocation} from '../../../interfaces/IEmployerLocation';

@Component({
  selector: 'app-company-locations',
  templateUrl: './company-locations.component.html',
  styleUrls: ['./company-locations.component.css']
})
export class CompanyLocationsComponent implements OnInit, OnDestroy {
  headquartersId = '';
  companyLocationAddMode = false;
  companyLocationEditMode = false;
  addingOrEditingCompanyLocationInProgress = false;
  deletingCompanyLocationInProgress = false;
  settingCompanyLocationAsHeadquartersInProgress = false;
  currentEditedCompanyLocationElementIndex = -1;
  currentDeletedCompanyLocationElementIndex = -1;

  companyLocations: IEmployerLocation[] = [];
  companyLocationForm: FormGroup;
  companyLocation: IEmployerLocation;

  // SUBSCRIPTIONS
  currentEmployerSubscription: Subscription;
  confirmDeletionCompanyLocationSubscription: Subscription;

  constructor(private employerService: EmployerService) { }

  ngOnInit() {
    this.currentEmployerSubscription = this.employerService.getCurrentEmployer().subscribe(employer => {
      this.companyLocations = [];
      this.headquartersId = employer.headquartersId;
      this.companyLocations = employer.locations;
    });

    this.confirmDeletionCompanyLocationSubscription
      = this.employerService.confirmDeletionEmployerLocationElement$.subscribe((locationId) => {
      this.deletingCompanyLocationInProgress = true;
      this.employerService.deleteEmployerLocationElement(locationId)
        .subscribe().add(() => {
        this.deletingCompanyLocationInProgress = false;
        this.currentDeletedCompanyLocationElementIndex = -1;
      });
    });
  }

  submitChanges() {
    this.addingOrEditingCompanyLocationInProgress = true;
    this.assignFormValues();
    if (this.companyLocationAddMode) {
      this.employerService.addEmployerLocation(this.companyLocation)
        .subscribe(() => {
            this.cancelAddingOrEditingCompanyLocation();
          },
          error => console.log(error))
        .add(() => this.addingOrEditingCompanyLocationInProgress = false);
    } else {
      this.employerService.editEmployerLocation(this.companyLocation)
        .subscribe(() => {
          this.cancelAddingOrEditingCompanyLocation();
        })
        .add(() => this.addingOrEditingCompanyLocationInProgress = false);
    }
  }

  addCompanyLocation() {
    this.companyLocationEditMode = false;
    this.companyLocationAddMode = true;
    this.initializeNewForm();
  }

  cancelAddingOrEditingCompanyLocation() {
    this.companyLocationAddMode = false;
    this.companyLocationEditMode = false;
    this.currentEditedCompanyLocationElementIndex = -1;
    this.resetForm();
  }

  deleteCompanyLocationElement(elementIndex: number) {
    this.currentDeletedCompanyLocationElementIndex = elementIndex;
    this.employerService.tryDeleteEmployerLocationElement(this.companyLocations[elementIndex].employerLocationId);
  }

  editCompanyLocationElement(elementIndex: number) {
    this.companyLocationAddMode = false;
    this.currentEditedCompanyLocationElementIndex = elementIndex;
    this.companyLocationEditMode = true;
    this.initializeEditForm(elementIndex);
  }

  isCompanyLocationElementEdited(elementIndex: number) {
    return this.currentEditedCompanyLocationElementIndex === elementIndex;
  }

  setAsHeadquarters(locationElementNumber: number) {
    this.settingCompanyLocationAsHeadquartersInProgress = true;
    this.employerService.setHeadquarters(this.companyLocations[locationElementNumber].employerLocationId)
      .subscribe(() => this.cancelAddingOrEditingCompanyLocation())
      .add(() => this.settingCompanyLocationAsHeadquartersInProgress = false);
  }

  ngOnDestroy(): void {
    this.currentEmployerSubscription.unsubscribe();
    this.confirmDeletionCompanyLocationSubscription.unsubscribe();
  }

  // Private
  private assignFormValues() {
    this.companyLocation = {
      employerLocationId: this.companyLocationEditMode
        ? this.companyLocations[this.currentEditedCompanyLocationElementIndex].employerLocationId
        : null,
      country: this.companyLocationForm.get('country').value,
      city: this.companyLocationForm.get('city').value,
      street: this.companyLocationForm.get('street').value,
      apartmentNumber: this.companyLocationForm.get('apartmentNumber').value,
      flatNumber: this.companyLocationForm.get('flatNumber').value,
      postalCode: this.companyLocationForm.get('postalCode').value,
    };
  }

  private initializeNewForm() {
    this.companyLocationForm = new FormGroup({
      'country': new FormControl(null, Validators.required),
      'city': new FormControl(null, Validators.required),
      'street': new FormControl(null, Validators.required),
      'apartmentNumber': new FormControl(null, Validators.required),
      'flatNumber': new FormControl(null),
      'postalCode': new FormControl(null, Validators.required),
    });
  }

  private initializeEditForm(elementIndex: number) {
    this.companyLocationForm = new FormGroup({
      'country': new FormControl(this.companyLocations[elementIndex].country, Validators.required),
      'city': new FormControl(this.companyLocations[elementIndex].city, Validators.required),
      'street': new FormControl(this.companyLocations[elementIndex].street, Validators.required),
      'apartmentNumber': new FormControl(this.companyLocations[elementIndex].apartmentNumber, Validators.required),
      'flatNumber': new FormControl(this.companyLocations[elementIndex].flatNumber),
      'postalCode': new FormControl(this.companyLocations[elementIndex].postalCode, Validators.required),
    });
  }

  private resetForm() {
    this.companyLocationForm.reset();
  }
}
