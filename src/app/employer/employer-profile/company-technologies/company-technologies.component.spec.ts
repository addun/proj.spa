import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyTechnologiesComponent } from './company-technologies.component';

describe('CompanyTechnologiesComponent', () => {
  let component: CompanyTechnologiesComponent;
  let fixture: ComponentFixture<CompanyTechnologiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyTechnologiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyTechnologiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
