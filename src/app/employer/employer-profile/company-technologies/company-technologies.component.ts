import {Component, OnDestroy, OnInit} from '@angular/core';
import {debounceTime, distinctUntilChanged, mergeMap} from 'rxjs/internal/operators';
import {Subscription} from 'rxjs';
import {AutocompleteService} from '../../../shared/services/autocomplete.service';
import {FormControl, FormGroup} from '@angular/forms';
import {IEmployerTechnology} from '../../../interfaces/IEmployerTechnology';
import {EmployerService} from '../../employer.service';

@Component({
  selector: 'app-company-technologies',
  templateUrl: './company-technologies.component.html',
  styleUrls: ['./company-technologies.component.css']
})
export class CompanyTechnologiesComponent implements OnInit, OnDestroy {
  companyTechnologyAddMode = false;
  addingCompanyTechnologyInProgress = false;
  deletingCompanyTechnologyInProgress = false;
  deletingElementIndex = -1;

  autocompletionVisible = false;
  technologyAutocompleteOptions: string[] = [];

  companyTechnologies: IEmployerTechnology[] = [];
  addCompanyTechnologyForm: FormGroup;
  employerTechnology: IEmployerTechnology;

  // SUBSCRIPTIONS
  currentEmployeeSubscription: Subscription;
  confirmDeletionEmployeeTechnologySubscription: Subscription;

  constructor(private employerService: EmployerService,
              private autocompleteService: AutocompleteService) { }

  ngOnInit() {
    this.initializeNewForm();

    this.currentEmployeeSubscription = this.employerService.getCurrentEmployer().subscribe(employer => {
      this.companyTechnologies = employer.technologies;
    });

    this.confirmDeletionEmployeeTechnologySubscription
      = this.employerService.confirmDeletionEmployerTechnologyElement$.subscribe((skillId) => {
      this.deletingCompanyTechnologyInProgress = true;
      this.employerService.deleteEmployerTechnologyElement(skillId)
        .subscribe().add(() => {
          this.deletingCompanyTechnologyInProgress = false;
          this.deletingElementIndex = -1;
        });
    });
  }

  onClickAutocompleteOption(index: number) {
    this.addCompanyTechnologyForm.controls.technologyName
      .patchValue(this.technologyAutocompleteOptions[index], {emitEvent: false});
    this.autocompletionVisible = false;
  }

  submitAddingEmployeeTechnology() {
    this.addingCompanyTechnologyInProgress = true;
    this.assignFormValues();

    this.employerService.addEmployerTechnology(this.employerTechnology)
      .subscribe(() => {
          this.cancelAddingEmployeeTechnology();
        },
        error => console.log(error))
      .add(() => this.addingCompanyTechnologyInProgress = false);
  }

  addEmployeeTechnology() {
    this.companyTechnologyAddMode = true;
    this.initializeNewForm();
  }

  cancelAddingEmployeeTechnology() {
    this.companyTechnologyAddMode = false;
    this.resetForm();
  }

  deleteEmployeeTechnologyElement(elementIndex: number) {
    this.deletingElementIndex = elementIndex;
    this.employerService.tryDeleteEmployerTechnologyElement(this.companyTechnologies[elementIndex].technologyId);
  }

  ngOnDestroy(): void {
    this.currentEmployeeSubscription.unsubscribe();
    this.confirmDeletionEmployeeTechnologySubscription.unsubscribe();
  }

  // PRIVATE
  private assignFormValues() {
    this.employerTechnology = {
      'technologyId': null,
      'technologyName': this.addCompanyTechnologyForm.get('technologyName').value,
    };
  }

  private initializeAutocompletion() {
    // AUTOCOMPLETE SET UP
    this.addCompanyTechnologyForm.controls.technologyName.valueChanges
      .pipe(debounceTime(500), distinctUntilChanged())
      .pipe(mergeMap((data) => {
        if (data === '' || data === null) {
          this.autocompletionVisible = false;
          return [];
        }
        return this.autocompleteService.autocompleteSkills(data);
      }))
      .subscribe((data) => {
        this.technologyAutocompleteOptions = data;
        if (this.technologyAutocompleteOptions.length > 0) {
          this.autocompletionVisible = true;
        } else {
          this.autocompletionVisible = false;
        }
      });
  }

  private initializeNewForm() {
    this.addCompanyTechnologyForm = new FormGroup({
      technologyName: new FormControl(null),
    });
    this.initializeAutocompletion();
  }

  private resetForm() {
    this.addCompanyTechnologyForm.reset();
  }
}
