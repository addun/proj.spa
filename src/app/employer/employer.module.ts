import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EmployerComponent} from './employer-profile/employer.component';
import {EmployerJobOffersComponent} from './employer-job-offers/employer-job-offers.component';
import {AccountModule} from '../account/account.module';
import { CompanyDataComponent } from './employer-profile/company-data/company-data.component';
import { CompanyLocationsComponent } from './employer-profile/company-locations/company-locations.component';
import { CompanyTechnologiesComponent } from './employer-profile/company-technologies/company-technologies.component';
import { CompanyBenefitsComponent } from './employer-profile/company-benefits/company-benefits.component';
import {ReactiveFormsModule} from '@angular/forms';
import { AngularEditorModule } from '@kolkov/angular-editor';
import {HttpClientModule} from '@angular/common/http';
import { AddJobOfferComponent } from './employer-job-offers/add-job-offer/add-job-offer.component';
import { JobOfferTabComponent } from './employer-job-offers/job-offer-tab/job-offer-tab.component';
import { JobDetailsComponent } from './employer-job-offers/job-details/job-details.component';
import { EditJobOfferComponent } from './employer-job-offers/edit-job-offer/edit-job-offer.component';
import {SharedModule} from '../shared/shared.module';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {ModalComponent} from '../shared/components/modal/modal.component';
import { EmployerPublicProfileComponent } from './employer-public-profile/employer-public-profile.component';

@NgModule({
  imports: [
    CommonModule,
    AccountModule,
    ReactiveFormsModule,
    HttpClientModule,
    AngularEditorModule,
    SharedModule,
  ],
  declarations: [
    EmployerComponent,
    EmployerJobOffersComponent,
    CompanyDataComponent,
    CompanyLocationsComponent,
    CompanyTechnologiesComponent,
    CompanyBenefitsComponent,
    AddJobOfferComponent,
    JobOfferTabComponent,
    JobDetailsComponent,
    EditJobOfferComponent,
    EmployerPublicProfileComponent
  ],
  providers: [
    NgbActiveModal
  ],
  entryComponents: [
    ModalComponent
  ]
})
export class EmployerModule { }
