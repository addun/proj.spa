import {Component, OnDestroy, OnInit} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {debounceTime, distinctUntilChanged, mergeMap} from 'rxjs/internal/operators';
import {AutocompleteService} from '../../shared/services/autocomplete.service';
import {EmployerService} from '../employer.service';
import {IEmployerJobOffer} from '../../interfaces/IEmployerJobOffer';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-employer-job-offers',
  templateUrl: './employer-job-offers.component.html',
  styleUrls: ['./employer-job-offers.component.css']
})
export class EmployerJobOffersComponent implements OnInit, OnDestroy {
  jobOffers: IEmployerJobOffer[] = [];

  employerSubscription: Subscription;

  constructor(private router: Router, private employerService: EmployerService) { }

  ngOnInit() {
    this.employerSubscription = this.employerService.getCurrentEmployer()
      .subscribe(emp => {
        this.jobOffers = emp.jobOffers;
      });
  }

  addNewOffer() {
    this.router.navigate(['/employer/job-offers/new']);
  }

  ngOnDestroy(): void {
    this.employerSubscription.unsubscribe();
  }
}
