import {Component, Input, OnInit} from '@angular/core';
import {IEmployerJobOffer} from '../../../interfaces/IEmployerJobOffer';
import {Router} from '@angular/router';

@Component({
  selector: 'app-job-offer-tab',
  templateUrl: './job-offer-tab.component.html',
  styleUrls: ['./job-offer-tab.component.css']
})
export class JobOfferTabComponent implements OnInit {
  @Input() jobOffer: IEmployerJobOffer;
  @Input() jobOfferUrlPrefix: string;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  jobDetails(offerId: string) {
    this.router.navigate([this.jobOfferUrlPrefix + '/' + offerId]);
  }
}
