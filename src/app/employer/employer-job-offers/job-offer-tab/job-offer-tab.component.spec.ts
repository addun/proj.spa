import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobOfferTabComponent } from './job-offer-tab.component';

describe('JobOfferTabComponent', () => {
  let component: JobOfferTabComponent;
  let fixture: ComponentFixture<JobOfferTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobOfferTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobOfferTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
