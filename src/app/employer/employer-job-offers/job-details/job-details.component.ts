import {Component, OnDestroy, OnInit} from '@angular/core';
import {EmployerService} from '../../employer.service';
import {ActivatedRoute, Router} from '@angular/router';
import {IEmployerJobOffer} from '../../../interfaces/IEmployerJobOffer';
import {ModalComponent} from '../../../shared/components/modal/modal.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Subscription} from 'rxjs';
import {IResumeData} from '../../../interfaces/IResumeData';

@Component({
  selector: 'app-job-applicants',
  templateUrl: './job-details.component.html',
  styleUrls: ['./job-details.component.css']
})
export class JobDetailsComponent implements OnInit, OnDestroy {
  jobOffer: IEmployerJobOffer = null;
  deletingJobOfferInProgress = false;

  currentEmployerSubscription: Subscription;

  constructor(private router: Router,
              private activatedRoute: ActivatedRoute,
              private employerService: EmployerService,
              private modalService: NgbModal) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      const offerId = params['id'];
      this.currentEmployerSubscription = this.employerService.getCurrentEmployer()
        .subscribe(employer => {
          const offer = employer.jobOffers.find(offerItem => offerItem.offerId === offerId);
          if (!offer) {
            this.router.navigate(['/not-found']);
          }
          this.jobOffer = offer;
          console.log(this.jobOffer);
        });
    });
  }

  downloadCV(offerId: string, applicantId: string) {
    this.employerService.getCV(offerId, applicantId)
      .subscribe((resumeData: IResumeData) => {
        console.log(resumeData);
        this.handleDownloadingCV(resumeData);
      });
  }

  onBackButtonClick() {
    this.router.navigate(['/employer/job-offers']);
  }

  onEditButtonClick() {
    this.router.navigate(['/employer/job-offers/edit/' + this.jobOffer.offerId]);
  }

  onDeleteJobOfferClick() {
    const modalRef = this.modalService.open(ModalComponent);
    modalRef.componentInstance.message = 'Czy na pewno chcesz usunąć ofertę pracy?';
    modalRef.componentInstance.closeButtonTitle = 'Usuń';
    modalRef.componentInstance.dismissButtonTitle = 'Anuluj';

    modalRef.result.then(() => {
      this.deletingJobOfferInProgress = true;
      this.employerService.deleteJobOffer(this.jobOffer.offerId)
        .subscribe(() => {
          this.router.navigate(['/employer/job-offers']);
        }).add(() => this.deletingJobOfferInProgress = false);
    }, () => {});
  }

  ngOnDestroy(): void {
    this.currentEmployerSubscription.unsubscribe();
  }

  redirectToApplicantProfile(applicantId: string) {
    console.log(applicantId);
    this.router.navigate(['/applicant/' + applicantId]);
  }

  private handleDownloadingCV(resumeData: IResumeData) {
    const byteCharacters = atob(resumeData.content);

    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }

    const byteArray = new Uint8Array(byteNumbers);

    const blob = new Blob([byteArray], { type: resumeData.contentType });
    const blobUrl = window.URL.createObjectURL(blob);
    const anchor = document.createElement('a');

    anchor.download = resumeData.fileName + '.' + resumeData.contentTypeShort;
    anchor.href = blobUrl;
    anchor.click();
  }
}
