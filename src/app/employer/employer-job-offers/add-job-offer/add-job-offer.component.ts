import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AngularEditorConfig} from '@kolkov/angular-editor';
import {Router} from '@angular/router';
import { applicationJobExperience } from '../../../shared/applicationJobExperienceValues';
import { applicationCurrencies } from '../../../shared/applicationCurrencies';
import {AutocompleteService} from '../../../shared/services/autocomplete.service';
import {debounceTime, distinctUntilChanged, mergeMap} from 'rxjs/internal/operators';
import {Subject} from 'rxjs';
import {IEmployerJobOffer} from '../../../interfaces/IEmployerJobOffer';
import {EmployerService} from '../../employer.service';

@Component({
  selector: 'app-add-job-offer',
  templateUrl: './add-job-offer.component.html',
  styleUrls: ['./add-job-offer.component.css']
})
export class AddJobOfferComponent implements OnInit {
  jobOffer: IEmployerJobOffer;

  technologyInputEvent = new Subject<string>();
  technologyInputValue = '';

  autocompletionVisible = false;
  technologyAutocompleteOptions: string[] = [];

  APPLICATION_JOB_EXPERIENCE_VALUES = applicationJobExperience;
  APPLICATION_CURRENCY_VALUES = applicationCurrencies;

  newJobOfferForm: FormGroup;
  addingJobOfferInProgress = false;

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '10.2rem',
    maxHeight: '10.2rem',
    placeholder: 'Job description...'
  };

  constructor(private router: Router, private autocompleteService: AutocompleteService, private employerService: EmployerService) { }

  ngOnInit() {
    this.initializeJobOffer();
    this.initializeForm();
  }

  addJobOffer() {
    this.addingJobOfferInProgress = true;
    this.assignFormValues();
    this.employerService.addJobOffer(this.jobOffer)
      .subscribe(() => {
        this.router.navigate(['/employer/job-offers']);
      })
      .add(() => this.addingJobOfferInProgress = false);
  }

  addSkill() {
    if (!(this.technologyInputValue === null || this.technologyInputValue.match(/^ *$/) !== null)
        && !(this.jobOffer.skills.map(x => x.toLowerCase()).includes(this.technologyInputValue.toLowerCase()))) {
          this.jobOffer.skills.push(this.technologyInputValue);
    }

    this.technologyInputValue = '';
    this.autocompletionVisible = false;
  }

  onCancel() {
    this.router.navigate(['/employer/job-offers']);
  }

  onClickAutocompleteOption(index: number) {
    this.technologyInputValue = this.technologyAutocompleteOptions[index];
    this.autocompletionVisible = false;
  }

  onTechnologyInput($event: any) {
    this.technologyInputValue = $event.srcElement.value;
    this.technologyInputEvent.next($event.srcElement.value);
  }

  isFormValid(): boolean {
    return this.newJobOfferForm.valid && this.jobOffer.skills.length > 0;
  }

  // Private
  private assignFormValues() {
    this.jobOffer = {
      offerId: null,
      title: this.newJobOfferForm.get('title').value,
      country: this.newJobOfferForm.get('country').value,
      city: this.newJobOfferForm.get('city').value,
      address: this.newJobOfferForm.get('address').value,
      creationDate: null,
      salaryFrom: this.newJobOfferForm.get('salaryFrom').value,
      salaryTo: this.newJobOfferForm.get('salaryTo').value,
      currency: this.newJobOfferForm.get('currency').value,
      workTime: this.newJobOfferForm.get('workTime').value,
      invoiceType: this.newJobOfferForm.get('invoiceType').value,
      experienceLevel: this.newJobOfferForm.get('experienceLevel').value,
      remote: this.newJobOfferForm.get('remote').value,
      offerDescription: this.newJobOfferForm.get('offerDescription').value,
      employer: null,
      applicants: [],
      skills: this.jobOffer.skills
    };
  }

  private initializeAutocompletion() {
    // AUTOCOMPLETE SET UP
    this.technologyInputEvent
      .pipe(debounceTime(500), distinctUntilChanged())
      .pipe(mergeMap((data) => {
        if (data === '' || data === null) {
          this.autocompletionVisible = false;
          return [];
        }
        return this.autocompleteService.autocompleteSkills(data);
      }))
      .subscribe((data) => {
        this.technologyAutocompleteOptions = data;
        if (this.technologyAutocompleteOptions.length > 0) {
          this.autocompletionVisible = true;
        } else {
          this.autocompletionVisible = false;
        }
      });
  }

  private initializeForm() {
    this.newJobOfferForm = new FormGroup({
      'title': new FormControl(null, Validators.required),
      'country': new FormControl(null, Validators.required),
      'city': new FormControl(null, Validators.required),
      'salaryFrom': new FormControl(null, [Validators.required, Validators.min(0)]),
      'salaryTo': new FormControl(null, [Validators.required, Validators.min(0)]),
      'currency': new FormControl(this.APPLICATION_CURRENCY_VALUES.PLN, Validators.required),
      'workTime': new FormControl(null, Validators.required),
      'invoiceType': new FormControl(null, Validators.required),
      'experienceLevel': new FormControl(this.APPLICATION_JOB_EXPERIENCE_VALUES.ANY, Validators.required),
      'offerDescription': new FormControl(null, Validators.required),
      'address': new FormControl(null, Validators.required),
      'remote': new FormControl(null, Validators.required),
    });
    this.initializeAutocompletion();
  }

  private initializeJobOffer() {
    this.jobOffer = {
      offerId: '',
      title: '',
      country: '',
      city: '',
      address: '',
      creationDate: '',
      salaryFrom: '',
      salaryTo: '',
      currency: '',
      workTime: '',
      invoiceType: '',
      experienceLevel: '',
      remote: '',
      offerDescription: '',
      employer: null,
      applicants: [],
      skills: []
    };
  }
}
