import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployerJobOffersComponent } from './employer-job-offers.component';

describe('EmployerJobOffersComponent', () => {
  let component: EmployerJobOffersComponent;
  let fixture: ComponentFixture<EmployerJobOffersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployerJobOffersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployerJobOffersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
