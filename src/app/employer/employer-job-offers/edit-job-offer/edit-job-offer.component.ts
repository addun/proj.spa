import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AngularEditorConfig} from '@kolkov/angular-editor';
import {debounceTime, distinctUntilChanged, mergeMap} from 'rxjs/internal/operators';
import {AutocompleteService} from '../../../shared/services/autocomplete.service';
import {Subject, Subscription} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {applicationJobExperience} from '../../../shared/applicationJobExperienceValues';
import {EmployerService} from '../../employer.service';
import {IEmployerJobOffer} from '../../../interfaces/IEmployerJobOffer';
import {applicationCurrencies} from '../../../shared/applicationCurrencies';

@Component({
  selector: 'app-edit-job-offer',
  templateUrl: './edit-job-offer.component.html',
  styleUrls: ['./edit-job-offer.component.css']
})
export class EditJobOfferComponent implements OnInit, OnDestroy {
  jobOffer: IEmployerJobOffer = null;

  technologyInputEvent = new Subject<string>();
  technologyInputValue = '';

  autocompletionVisible = false;
  technologyAutocompleteOptions: string[] = [];

  APPLICATION_JOB_EXPERIENCE_VALUES = applicationJobExperience;
  APPLICATION_CURRENCY_VALUES = applicationCurrencies;

  editJobOfferForm: FormGroup;
  editingJobOfferInProgress = false;

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '10rem',
    minHeight: '5rem',
    placeholder: 'Job description...',
  };

  currentEmployerSubscription: Subscription;

  constructor(private router: Router,
              private activatedRoute: ActivatedRoute,
              private autocompleteService: AutocompleteService,
              private employerService: EmployerService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      const offerId = params['id'];
      this.currentEmployerSubscription = this.employerService.getCurrentEmployer()
        .subscribe(employer => {
          console.log(employer);
          console.log(params['id']);
          const offer = employer.jobOffers.find(offerItem => offerItem.offerId === offerId);
          console.log(offer);
          if (!offer) {
            this.router.navigate(['/not-found']);
          }
          this.jobOffer = offer;
          this.initializeForm();
        });
    });
  }

  editJobOffer() {
    this.editingJobOfferInProgress = true;
    this.assignFormValues();
    this.employerService.editJobOffer(this.jobOffer)
      .subscribe(() => {
        this.router.navigate(['/employer/job-offers']);
      })
      .add(() => this.editingJobOfferInProgress = false);
  }

  addSkill() {
    if (!(this.technologyInputValue === null || this.technologyInputValue.match(/^ *$/) !== null)
      && !(this.jobOffer.skills.map(x => x.toLowerCase()).includes(this.technologyInputValue.toLowerCase()))) {
      this.jobOffer.skills.push(this.technologyInputValue);
    }

    this.technologyInputValue = '';
    this.autocompletionVisible = false;
  }

  isFormValid() {
    return this.editJobOfferForm.valid && this.jobOffer.skills.length > 0;
  }

  onCancel() {
    this.router.navigate(['/employer/job-offers']);
  }

  onClickAutocompleteOption(index: number) {
    this.technologyInputValue = this.technologyAutocompleteOptions[index];
    this.autocompletionVisible = false;
  }

  onTechnologyInput($event: any) {
    this.technologyInputValue = $event.srcElement.value;
    this.technologyInputEvent.next($event.srcElement.value);
  }

  // Private
  private assignFormValues() {
    this.jobOffer = {
      offerId: this.jobOffer.offerId,
      title: this.editJobOfferForm.get('title').value,
      country: this.editJobOfferForm.get('country').value,
      city: this.editJobOfferForm.get('city').value,
      address: this.editJobOfferForm.get('address').value,
      creationDate: this.jobOffer.creationDate,
      salaryFrom: this.editJobOfferForm.get('salaryFrom').value,
      salaryTo: this.editJobOfferForm.get('salaryTo').value,
      currency: this.editJobOfferForm.get('currency').value,
      workTime: this.editJobOfferForm.get('workTime').value,
      invoiceType: this.editJobOfferForm.get('invoiceType').value,
      experienceLevel: this.editJobOfferForm.get('experienceLevel').value,
      remote: this.editJobOfferForm.get('remote').value,
      offerDescription: this.editJobOfferForm.get('offerDescription').value,
      employer: null,
      applicants: [],
      skills: this.jobOffer.skills
    };
  }

  private initializeForm() {
    this.editJobOfferForm = new FormGroup({
      'title': new FormControl(this.jobOffer.title, Validators.required),
      'country': new FormControl(this.jobOffer.country, Validators.required),
      'city': new FormControl(this.jobOffer.city, Validators.required),
      'salaryFrom': new FormControl(this.jobOffer.salaryFrom, [Validators.required, Validators.min(0)]),
      'salaryTo': new FormControl(this.jobOffer.salaryTo, [Validators.required, Validators.min(0)]),
      'currency': new FormControl(this.jobOffer.currency, Validators.required),
      'workTime': new FormControl(this.jobOffer.workTime, Validators.required),
      'invoiceType': new FormControl(this.jobOffer.invoiceType, Validators.required),
      'experienceLevel': new FormControl(this.jobOffer.experienceLevel, Validators.required),
      'offerDescription': new FormControl(this.jobOffer.offerDescription, Validators.required),
      'address': new FormControl(this.jobOffer.address, Validators.required),
      'remote': new FormControl(this.jobOffer.remote, Validators.required),
    });
    this.initializeAutocompletion();
  }

  private initializeAutocompletion() {
    // AUTOCOMPLETE SET UP
    this.technologyInputEvent
      .pipe(debounceTime(500), distinctUntilChanged())
      .pipe(mergeMap((data) => {
        if (data === '' || data === null) {
          this.autocompletionVisible = false;
          return [];
        }
        return this.autocompleteService.autocompleteSkills(data);
      }))
      .subscribe((data) => {
        this.technologyAutocompleteOptions = data;
        if (this.technologyAutocompleteOptions.length > 0) {
          this.autocompletionVisible = true;
        } else {
          this.autocompletionVisible = false;
        }
      });
  }

  ngOnDestroy(): void {
    this.currentEmployerSubscription.unsubscribe();
  }
}
