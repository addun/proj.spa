import {Injectable, OnDestroy} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {of, ReplaySubject, Subject, Subscription} from 'rxjs';
import {IEmployer} from '../interfaces/IEmployer';
import {AccountService} from '../account/account.service';
import {mergeMap} from 'rxjs/operators';
import {IEmployerData} from '../interfaces/IEmployerData';
import {IEmployerBenefit} from '../interfaces/IEmployerBenefit';
import {IEmployerLocation} from '../interfaces/IEmployerLocation';
import {IEmployerTechnology} from '../interfaces/IEmployerTechnology';
import {IEmployerJobOffer} from '../interfaces/IEmployerJobOffer';

@Injectable()
export class EmployerService implements OnDestroy {
  private baseUrl = 'http://localhost:5000/api/';
  // private baseUrl = 'https://justfindit-api.azurewebsites.net/api/';
  private headers: HttpHeaders;
  private currentEmployer$: ReplaySubject<IEmployer> = new ReplaySubject<IEmployer>(1);

  employerInitialized = false;

  confirmDeletionBenefitElement$: Subject<string> = new Subject<string>();
  confirmDeletionEmployerLocationElement$: Subject<string> = new Subject<string>();
  confirmDeletionEmployerTechnologyElement$: Subject<string> = new Subject<string>();

  tryDeleteBenefitElement$: Subject<string> = new Subject<string>();
  tryDeleteEmployerLocation$: Subject<string> = new Subject<string>();
  tryDeleteEmployerTechnology$: Subject<string> = new Subject<string>();

  accountServiceLogoutEventSubscription: Subscription;

  constructor(private httpClient: HttpClient, private accountService: AccountService) {
    this.headers = new HttpHeaders();

    this.accountServiceLogoutEventSubscription = this.accountService.userLogout$
      .subscribe(() => this.resetState());
  }

  addEmployerBenefit(employerBenefit: IEmployerBenefit) {
    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.accountService.getToken());
    const request = this.httpClient
      .post(this.baseUrl + 'employer/benefit',
        { userId: this.accountService.currentUser.id, benefit: employerBenefit },
        { headers: requestAuthHeaders });

    return request.pipe(
      mergeMap(() => {
        return this.httpClient.get<IEmployer>
        (this.baseUrl + 'employer/' + this.accountService.currentUser.id, {headers: requestAuthHeaders});
      }))
      .pipe(mergeMap((data: IEmployer) => {
        this.currentEmployer$.next(data);
        return of([]);
      }));
  }

  addJobOffer(jobOffer: IEmployerJobOffer) {
    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.accountService.getToken());
    const request = this.httpClient
      .post(this.baseUrl + 'employer/job-offer',
        { userId: this.accountService.currentUser.id, jobOffer: jobOffer },
        { headers: requestAuthHeaders });

    return request.pipe(
      mergeMap(() => {
        return this.httpClient.get<IEmployer>
        (this.baseUrl + 'employer/' + this.accountService.currentUser.id, {headers: requestAuthHeaders});
      }))
      .pipe(mergeMap((data: IEmployer) => {
        this.currentEmployer$.next(data);
        return of([]);
      }));
  }

  addEmployerLocation(employerLocation: IEmployerLocation) {
    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.accountService.getToken());
    const request = this.httpClient
      .post(this.baseUrl + 'employer/location',
        { userId: this.accountService.currentUser.id, location: employerLocation },
        { headers: requestAuthHeaders });

    return request.pipe(
      mergeMap(() => {
        return this.httpClient.get<IEmployer>
        (this.baseUrl + 'employer/' + this.accountService.currentUser.id, {headers: requestAuthHeaders});
      }))
      .pipe(mergeMap((data: IEmployer) => {
        this.currentEmployer$.next(data);
        return of([]);
      }));
  }

  addEmployerTechnology(employerTechnology: IEmployerTechnology) {
    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.accountService.getToken());
    const request = this.httpClient
      .post(this.baseUrl + 'employer/technology',
        { userId: this.accountService.currentUser.id, technology: employerTechnology },
        { headers: requestAuthHeaders });

    return request.pipe(
      mergeMap(() => {
        return this.httpClient.get<IEmployer>
        (this.baseUrl + 'employer/' + this.accountService.currentUser.id, {headers: requestAuthHeaders});
      }))
      .pipe(mergeMap((data: IEmployer) => {
        this.currentEmployer$.next(data);
        return of([]);
      }));
  }

  confirmDeletionBenefitElement(benefitElementId: string) {
    this.confirmDeletionBenefitElement$.next(benefitElementId);
  }

  confirmDeletionEmployerLocationElement(employerLocationId: string) {
    this.confirmDeletionEmployerLocationElement$.next(employerLocationId);
  }

  confirmDeletionEmployerTechnologyElement(employerTechnologyId: string) {
    this.confirmDeletionEmployerTechnologyElement$.next(employerTechnologyId);
  }

  deleteBenefitElement(benefitId: string) {
    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.accountService.getToken());
    const request = this.httpClient
      .delete(this.baseUrl + 'employer/benefit/delete/' + this.accountService.currentUser.id + '/' + benefitId,
        { headers: requestAuthHeaders });

    return request.pipe(
      mergeMap(() => {
        return this.httpClient.get<IEmployer>
        (this.baseUrl + 'employer/' + this.accountService.currentUser.id, {headers: requestAuthHeaders});
      }))
      .pipe(mergeMap((data: IEmployer) => {
        this.currentEmployer$.next(data);
        return of([]);
      }));
  }

  deleteEmployerLocationElement(locationId: string) {
    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.accountService.getToken());
    const request = this.httpClient
      .delete(this.baseUrl + 'employer/location/delete/' + this.accountService.currentUser.id + '/' + locationId,
        { headers: requestAuthHeaders });

    return request.pipe(
      mergeMap(() => {
        return this.httpClient.get<IEmployer>
        (this.baseUrl + 'employer/' + this.accountService.currentUser.id, {headers: requestAuthHeaders});
      }))
      .pipe(mergeMap((data: IEmployer) => {
        this.currentEmployer$.next(data);
        return of([]);
      }));
  }

  deleteEmployerTechnologyElement(technologyId: string) {
    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.accountService.getToken());
    const request = this.httpClient
      .delete(this.baseUrl + 'employer/technology/delete/' + this.accountService.currentUser.id + '/' + technologyId,
        { headers: requestAuthHeaders });

    return request.pipe(
      mergeMap(() => {
        return this.httpClient.get<IEmployer>
        (this.baseUrl + 'employer/' + this.accountService.currentUser.id, {headers: requestAuthHeaders});
      }))
      .pipe(mergeMap((data: IEmployer) => {
        this.currentEmployer$.next(data);
        return of([]);
      }));
  }

  deleteJobOffer(jobOfferId: string) {
    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.accountService.getToken());
    const request = this.httpClient
      .delete(this.baseUrl + 'employer/job-offer/delete/' + this.accountService.currentUser.id + '/' + jobOfferId,
        { headers: requestAuthHeaders });

    return request.pipe(
      mergeMap(() => {
        return this.httpClient.get<IEmployer>
        (this.baseUrl + 'employer/' + this.accountService.currentUser.id, {headers: requestAuthHeaders});
      }))
      .pipe(mergeMap((data: IEmployer) => {
        this.currentEmployer$.next(data);
        return of([]);
      }));
  }

  editEmployerData(employerData: IEmployerData) {
    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.accountService.getToken());
    const request = this.httpClient
      .post(this.baseUrl + 'employer/employer-data/edit',
        { userId: this.accountService.currentUser.id, employerData: employerData },
        { headers: requestAuthHeaders });

    return request.pipe(
      mergeMap(() => {
        return this.httpClient.get<IEmployer>
        (this.baseUrl + 'employer/' + this.accountService.currentUser.id, {headers: requestAuthHeaders});
      }))
      .pipe(mergeMap((data: IEmployer) => {
        this.currentEmployer$.next(data);
        return of([]);
      }));
  }

  editEmployerLocation(employerLocation: IEmployerLocation) {
    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.accountService.getToken());
    const request = this.httpClient
      .post(this.baseUrl + 'employer/location/edit',
        { userId: this.accountService.currentUser.id, location: employerLocation },
        { headers: requestAuthHeaders });

    return request.pipe(
      mergeMap(() => {
        return this.httpClient.get<IEmployer>
        (this.baseUrl + 'employer/' + this.accountService.currentUser.id, {headers: requestAuthHeaders});
      }))
      .pipe(mergeMap((data: IEmployer) => {
        this.currentEmployer$.next(data);
        return of([]);
      }));
  }

  editEmployerBenefit(employerBenefit: IEmployerBenefit) {
    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.accountService.getToken());
    const request = this.httpClient
      .post(this.baseUrl + 'employer/benefit/edit',
        { userId: this.accountService.currentUser.id, benefit: employerBenefit },
        { headers: requestAuthHeaders });

    return request.pipe(
      mergeMap(() => {
        return this.httpClient.get<IEmployer>
        (this.baseUrl + 'employer/' + this.accountService.currentUser.id, {headers: requestAuthHeaders});
      }))
      .pipe(mergeMap((data: IEmployer) => {
        this.currentEmployer$.next(data);
        return of([]);
      }));
  }

  editInternshipDescription(description: string) {
    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.accountService.getToken());
    const request = this.httpClient
      .post(this.baseUrl + 'employer/internships/edit',
        { userId: this.accountService.currentUser.id, description: description },
        { headers: requestAuthHeaders });

    return request.pipe(
      mergeMap(() => {
        return this.httpClient.get<IEmployer>
        (this.baseUrl + 'employer/' + this.accountService.currentUser.id, {headers: requestAuthHeaders});
      }))
      .pipe(mergeMap((data: IEmployer) => {
        this.currentEmployer$.next(data);
        return of([]);
      }));
  }

  editJobOffer(jobOffer: IEmployerJobOffer) {
    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.accountService.getToken());
    const request = this.httpClient
      .post(this.baseUrl + 'employer/job-offer/edit',
        { userId: this.accountService.currentUser.id, jobOffer: jobOffer },
        { headers: requestAuthHeaders });

    return request.pipe(
      mergeMap(() => {
        return this.httpClient.get<IEmployer>
        (this.baseUrl + 'employer/' + this.accountService.currentUser.id, {headers: requestAuthHeaders});
      }))
      .pipe(mergeMap((data: IEmployer) => {
        this.currentEmployer$.next(data);
        console.log('emit edited');
        return of([]);
      }));
  }

  editWhatsWeCreatingDescription(description: string) {
    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.accountService.getToken());
    const request = this.httpClient
      .post(this.baseUrl + 'employer/whats-we-creating/edit',
        { userId: this.accountService.currentUser.id, description: description },
        { headers: requestAuthHeaders });

    return request.pipe(
      mergeMap(() => {
        return this.httpClient.get<IEmployer>
        (this.baseUrl + 'employer/' + this.accountService.currentUser.id, {headers: requestAuthHeaders});
      }))
      .pipe(mergeMap((data: IEmployer) => {
        this.currentEmployer$.next(data);
        return of([]);
      }));
  }

  editWorkingWithUsDescription(description: string) {
    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.accountService.getToken());
    const request = this.httpClient
      .post(this.baseUrl + 'employer/working-with-us/edit',
        { userId: this.accountService.currentUser.id, description: description },
        { headers: requestAuthHeaders });

    return request.pipe(
      mergeMap(() => {
        return this.httpClient.get<IEmployer>
        (this.baseUrl + 'employer/' + this.accountService.currentUser.id, {headers: requestAuthHeaders});
      }))
      .pipe(mergeMap((data: IEmployer) => {
        this.currentEmployer$.next(data);
        return of([]);
      }));
  }

  getCurrentEmployer() {
    if (this.employerInitialized) {
      return this.currentEmployer$;
    } else {
      const subscription = this.initializeCurrentlyLoggedEmployer()
        .subscribe((data: IEmployer) => {
          this.currentEmployer$.next(data);
        }, error => {
          this.employerInitialized = false;
        }).add(() => subscription.unsubscribe());

      this.employerInitialized = true;
      return this.currentEmployer$;
    }
  }

  getEmployerProfileById(id: string) {
    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.accountService.getToken());
    return this.httpClient
      .get(this.baseUrl + 'employee/employer-profile/' + id, { headers: requestAuthHeaders });
  }

  getCV(jobOfferId: string, applicantId: string) {
    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.accountService.getToken());
    return this.httpClient
      .get(this.baseUrl + 'employer/offer/' + this.accountService.currentUser.id + '/' + jobOfferId + '/' + applicantId + '/cv',
        { headers: requestAuthHeaders });
  }

  private initializeCurrentlyLoggedEmployer() {
    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.accountService.getToken());
    return this.httpClient
      .get(this.baseUrl + 'employer/' + this.accountService.currentUser.id, { headers: requestAuthHeaders });
  }

  setHeadquarters(locationId: string) {
    console.log(locationId);
    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.accountService.getToken());
    const request = this.httpClient
      .post(this.baseUrl + 'employer/headquarters',
        { userId: this.accountService.currentUser.id, locationId: locationId },
        { headers: requestAuthHeaders });

    return request.pipe(
      mergeMap(() => {
        return this.httpClient.get<IEmployer>
        (this.baseUrl + 'employer/' + this.accountService.currentUser.id, {headers: requestAuthHeaders});
      }))
      .pipe(mergeMap((data: IEmployer) => {
        this.currentEmployer$.next(data);
        return of([]);
      }));
  }

  tryDeleteBenefitElement(benefitElementId: string) {
    this.tryDeleteBenefitElement$.next(benefitElementId);
  }

  tryDeleteEmployerLocationElement(employerLocationId: string) {
    this.tryDeleteEmployerLocation$.next(employerLocationId);
  }

  tryDeleteEmployerTechnologyElement(employerTechnologyId: string) {
    this.tryDeleteEmployerTechnology$.next(employerTechnologyId);
  }

  private resetState() {
    this.currentEmployer$ = new ReplaySubject<IEmployer>(1);
    this.employerInitialized = false;
  }

  ngOnDestroy(): void {
    this.accountServiceLogoutEventSubscription.unsubscribe();
  }
}
