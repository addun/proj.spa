import {Injectable, OnDestroy} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AccountService} from '../account/account.service';
import {IEmployee} from '../interfaces/IEmployee';
import {Subject, of, ReplaySubject, Subscription} from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import {IEmployeePersonalData} from '../interfaces/IEmployeePersonalData';
import {IEmployeeJobExperience} from '../interfaces/IEmployeeJobExperience';
import {IEmployeeEducation} from '../interfaces/IEmployeeEducation';
import {IEmployeeCertificate} from '../interfaces/IEmployeeCertificate';
import {IEmployeeSkill} from '../interfaces/IEmployeeSkill';
import {IEmployeeLanguage} from '../interfaces/IEmployeeLanguage';
import {IEditEmployeeSkill} from '../interfaces/IEditEmployeeSkill';
import {IEditEmployeeLanguage} from '../interfaces/IEditEmployeeLanguage';

@Injectable()
export class EmployeeService implements OnDestroy {
  private baseUrl = 'http://localhost:5000/api/';
  // private baseUrl = 'https://justfindit-api.azurewebsites.net/api/';
  private headers: HttpHeaders;

  private currentEmployee$: ReplaySubject<IEmployee> = new ReplaySubject<IEmployee>(1);
  employeeInitialized = false;

  confirmDeletionCertificateElement$: Subject<string> = new Subject<string>();
  confirmDeletionEducationElement$: Subject<string> = new Subject<string>();
  confirmDeletionEmployeeLanguage$: Subject<string> = new Subject<string>();
  confirmDeletionEmployeeSkill$: Subject<string> = new Subject<string>();
  confirmDeletionProfessionalExperienceElement$: Subject<string> = new Subject<string>();

  tryDeleteCertificateElement$: Subject<string> = new Subject<string>();
  tryDeleteEducationElement$: Subject<string> = new Subject<string>();
  tryDeleteEmployeeLanguage$: Subject<string> = new Subject<string>();
  tryDeleteEmployeeSkill$: Subject<string> = new Subject<string>();
  tryDeleteProfessionalExperienceElement$: Subject<string> = new Subject<string>();

  accountServiceLogoutEventSubscription: Subscription;

  constructor(private httpClient: HttpClient, private accountService: AccountService) {
    this.headers = new HttpHeaders();
    this.accountServiceLogoutEventSubscription = accountService.userLogout$
      .subscribe(() => this.resetState());
  }

  addEmployeeCertificate(employeeCertificate: IEmployeeCertificate) {
    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.accountService.getToken());
    const request = this.httpClient
      .post(this.baseUrl + 'employee/certificate/',
        { userId: this.accountService.currentUser.id, course: employeeCertificate },
        { headers: requestAuthHeaders });

    return request.pipe(
      mergeMap(() => {
        return this.httpClient.get<IEmployee>
        (this.baseUrl + 'employee/' + this.accountService.currentUser.id, {headers: requestAuthHeaders});
      }))
      .pipe(mergeMap((data: IEmployee) => {
        this.currentEmployee$.next(data);
        return of([]);
      }));
  }

  addEmployeeEducation(employeeEducation: IEmployeeEducation) {
    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.accountService.getToken());
    const request = this.httpClient
      .post(this.baseUrl + 'employee/education/',
        { userId: this.accountService.currentUser.id, education: employeeEducation },
        { headers: requestAuthHeaders });

    return request.pipe(
      mergeMap(() => {
        return this.httpClient.get<IEmployee>
        (this.baseUrl + 'employee/' + this.accountService.currentUser.id, {headers: requestAuthHeaders});
      }))
      .pipe(mergeMap((data: IEmployee) => {
        this.currentEmployee$.next(data);
        return of([]);
      }));
  }

  addEmployeeLanguage(employeeLanguage: IEmployeeLanguage) {
    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.accountService.getToken());
    const request = this.httpClient
      .post(this.baseUrl + 'employee/language/',
        { userId: this.accountService.currentUser.id, language: employeeLanguage },
        { headers: requestAuthHeaders });

    return request.pipe(
      mergeMap(() => {
        return this.httpClient.get<IEmployee>
        (this.baseUrl + 'employee/' + this.accountService.currentUser.id, {headers: requestAuthHeaders});
      }))
      .pipe(mergeMap((data: IEmployee) => {
        this.currentEmployee$.next(data);
        return of([]);
      }));
  }

  addEmployeeProfessionalExperience(employeeJob: IEmployeeJobExperience) {
    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.accountService.getToken());
    const request = this.httpClient
      .post(this.baseUrl + 'employee/professional-experience/',
        { userId: this.accountService.currentUser.id, professionalExperience: employeeJob },
        { headers: requestAuthHeaders });

    return request.pipe(
      mergeMap(() => {
        return this.httpClient.get<IEmployee>
        (this.baseUrl + 'employee/' + this.accountService.currentUser.id, {headers: requestAuthHeaders});
      }))
      .pipe(mergeMap((data: IEmployee) => {
        this.currentEmployee$.next(data);
        return of([]);
      }));
  }

  addEmployeeSkill(employeeSkill: IEmployeeSkill) {
    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.accountService.getToken());
    const request = this.httpClient
      .post(this.baseUrl + 'employee/skill/',
        { userId: this.accountService.currentUser.id, skill: employeeSkill },
        { headers: requestAuthHeaders });

    return request.pipe(
      mergeMap(() => {
        return this.httpClient.get<IEmployee>
        (this.baseUrl + 'employee/' + this.accountService.currentUser.id, {headers: requestAuthHeaders});
      }))
      .pipe(mergeMap((data: IEmployee) => {
        this.currentEmployee$.next(data);
        return of([]);
      }));
  }

  addJobOfferToFavourites(offerId: string) {
    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.accountService.getToken());
    const request = this.httpClient
      .post(this.baseUrl + 'employee/job-offer/favourite/',
        { userId: this.accountService.currentUser.id, offerId: offerId },
        { headers: requestAuthHeaders });

    return request.pipe(
      mergeMap(() => {
        return this.httpClient.get<IEmployee>
        (this.baseUrl + 'employee/' + this.accountService.currentUser.id, {headers: requestAuthHeaders});
      }))
      .pipe(mergeMap((data: IEmployee) => {
        this.currentEmployee$.next(data);
        return of([]);
      }));
  }

  applyForJob(jobId: string, uploadFile: File) {
    const formData = new FormData();
    formData.append('userId', this.accountService.currentUser.id);
    formData.append('jobId', jobId);
    formData.append('uploadFile', uploadFile);

    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.accountService.getToken());
    const request = this.httpClient
      .post(this.baseUrl + 'employee/apply-for-job/',
        formData, { headers: requestAuthHeaders });

    return request.pipe(
      mergeMap(() => {
        return this.httpClient.get<IEmployee>
        (this.baseUrl + 'employee/' + this.accountService.currentUser.id, {headers: requestAuthHeaders});
      }))
      .pipe(mergeMap((data: IEmployee) => {
        this.currentEmployee$.next(data);
        return of([]);
      }));
  }

  confirmDeletionCertificateElement(certificateElementId: string) {
    this.confirmDeletionCertificateElement$.next(certificateElementId);
  }

  confirmDeletionEducationElement(educationElementId: string) {
    this.confirmDeletionEducationElement$.next(educationElementId);
  }

  confirmDeletionEmployeeLanguage(languageId: string) {
    this.confirmDeletionEmployeeLanguage$.next(languageId);
  }

  confirmDeletionEmployeeSkill(skillId: string) {
    this.confirmDeletionEmployeeSkill$.next(skillId);
  }

  confirmDeletionProfessionalExperienceElement(experienceElementId: string) {
    this.confirmDeletionProfessionalExperienceElement$.next(experienceElementId);
  }

  deleteCertificateElement(certificateId: string) {
    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.accountService.getToken());
    const request = this.httpClient
      .delete(this.baseUrl + 'employee/certificate/delete/' + this.accountService.currentUser.id + '/' + certificateId,
        { headers: requestAuthHeaders });

    return request.pipe(
      mergeMap(() => {
        return this.httpClient.get<IEmployee>
        (this.baseUrl + 'employee/' + this.accountService.currentUser.id, {headers: requestAuthHeaders});
      }))
      .pipe(mergeMap((data: IEmployee) => {
        this.currentEmployee$.next(data);
        return of([]);
      }));
  }

  deleteEducationElement(educationId: string) {
    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.accountService.getToken());
    const request = this.httpClient
      .delete(this.baseUrl + 'employee/education/delete/' + this.accountService.currentUser.id + '/' + educationId,
        { headers: requestAuthHeaders });

    return request.pipe(
      mergeMap(() => {
        return this.httpClient.get<IEmployee>
        (this.baseUrl + 'employee/' + this.accountService.currentUser.id, {headers: requestAuthHeaders});
      }))
      .pipe(mergeMap((data: IEmployee) => {
        this.currentEmployee$.next(data);
        return of([]);
      }));
  }

  deleteJobOfferFromFavourites(offerId: string) {
    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.accountService.getToken());
    const request = this.httpClient
      .delete(this.baseUrl + 'employee/job-offer/favourite/delete/' + this.accountService.currentUser.id + '/' + offerId,
        { headers: requestAuthHeaders });

    return request.pipe(
      mergeMap(() => {
        return this.httpClient.get<IEmployee>
        (this.baseUrl + 'employee/' + this.accountService.currentUser.id, {headers: requestAuthHeaders});
      }))
      .pipe(mergeMap((data: IEmployee) => {
        this.currentEmployee$.next(data);
        return of([]);
      }));
  }

  deleteProfessionalExperienceElement(employeeJobId: string) {
    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.accountService.getToken());
    const request = this.httpClient
      .delete(this.baseUrl + 'employee/professional-experience/delete/' + this.accountService.currentUser.id + '/' + employeeJobId,
        { headers: requestAuthHeaders });

    return request.pipe(
      mergeMap(() => {
        return this.httpClient.get<IEmployee>
        (this.baseUrl + 'employee/' + this.accountService.currentUser.id, {headers: requestAuthHeaders});
      }))
      .pipe(mergeMap((data: IEmployee) => {
        this.currentEmployee$.next(data);
        return of([]);
      }));
  }


  deleteEmployeeLanguage(languageId: string) {
    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.accountService.getToken());
    const request = this.httpClient
      .delete(this.baseUrl + 'employee/language/delete/' + this.accountService.currentUser.id + '/' + languageId,
        { headers: requestAuthHeaders });

    return request.pipe(
      mergeMap(() => {
        return this.httpClient.get<IEmployee>
        (this.baseUrl + 'employee/' + this.accountService.currentUser.id, {headers: requestAuthHeaders});
      }))
      .pipe(mergeMap((data: IEmployee) => {
        this.currentEmployee$.next(data);
        return of([]);
      }));
  }

  deleteEmployeeSkill(skillId: string) {
    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.accountService.getToken());
    const request = this.httpClient
      .delete(this.baseUrl + 'employee/skill/delete/' + this.accountService.currentUser.id + '/' + skillId,
        { headers: requestAuthHeaders });

    return request.pipe(
      mergeMap(() => {
        return this.httpClient.get<IEmployee>
        (this.baseUrl + 'employee/' + this.accountService.currentUser.id, {headers: requestAuthHeaders});
      }))
      .pipe(mergeMap((data: IEmployee) => {
        this.currentEmployee$.next(data);
        return of([]);
      }));
  }

  editEmployeeCertificate(employeeCertificate: IEmployeeCertificate) {
    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.accountService.getToken());
    const request = this.httpClient
      .post(this.baseUrl + 'employee/certificate/edit',
        { userId: this.accountService.currentUser.id, course: employeeCertificate },
        { headers: requestAuthHeaders });

    return request.pipe(
      mergeMap(() => {
        return this.httpClient.get<IEmployee>
        (this.baseUrl + 'employee/' + this.accountService.currentUser.id, {headers: requestAuthHeaders});
      }))
      .pipe(mergeMap((data: IEmployee) => {
        this.currentEmployee$.next(data);
        return of([]);
      }));
  }

  editEmployeeEducation(employeeEducation: IEmployeeEducation) {
    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.accountService.getToken());
    const request = this.httpClient
      .post(this.baseUrl + 'employee/education/edit',
        { userId: this.accountService.currentUser.id, education: employeeEducation },
        { headers: requestAuthHeaders });

    return request.pipe(
      mergeMap(() => {
        return this.httpClient.get<IEmployee>
        (this.baseUrl + 'employee/' + this.accountService.currentUser.id, {headers: requestAuthHeaders});
      }))
      .pipe(mergeMap((data: IEmployee) => {
        this.currentEmployee$.next(data);
        return of([]);
      }));
  }

  editEmployeeProfessionalExperience(employeeJob: IEmployeeJobExperience) {
    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.accountService.getToken());
    const request = this.httpClient
      .post(this.baseUrl + 'employee/professional-experience/edit',
        { userId: this.accountService.currentUser.id, professionalExperience: employeeJob },
        { headers: requestAuthHeaders });

    return request.pipe(
      mergeMap(() => {
        return this.httpClient.get<IEmployee>
        (this.baseUrl + 'employee/' + this.accountService.currentUser.id, {headers: requestAuthHeaders});
      }))
      .pipe(mergeMap((data: IEmployee) => {
        this.currentEmployee$.next(data);
        return of([]);
      }));
  }

  editEmployeeLanguage(employeeLanguage: IEditEmployeeLanguage) {
    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.accountService.getToken());
    const request = this.httpClient
      .post(this.baseUrl + 'employee/language/edit',
        { userId: this.accountService.currentUser.id, languageId: employeeLanguage.languageId, level: employeeLanguage.level },
        { headers: requestAuthHeaders });

    return request.pipe(
      mergeMap(() => {
        return this.httpClient.get<IEmployee>
        (this.baseUrl + 'employee/' + this.accountService.currentUser.id, {headers: requestAuthHeaders});
      }))
      .pipe(mergeMap((data: IEmployee) => {
        this.currentEmployee$.next(data);
        return of([]);
      }));
  }

  editEmployeeSkill(employeeSkill: IEditEmployeeSkill) {
    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.accountService.getToken());
    const request = this.httpClient
      .post(this.baseUrl + 'employee/skill/edit',
        { userId: this.accountService.currentUser.id, skillId: employeeSkill.skillId, level: employeeSkill.level },
        { headers: requestAuthHeaders });

    return request.pipe(
      mergeMap(() => {
        return this.httpClient.get<IEmployee>
        (this.baseUrl + 'employee/' + this.accountService.currentUser.id, {headers: requestAuthHeaders});
      }))
      .pipe(mergeMap((data: IEmployee) => {
        this.currentEmployee$.next(data);
        return of([]);
      }));
  }

  editPersonalData(personalData: IEmployeePersonalData) {
    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.accountService.getToken());
    const request = this.httpClient
      .post(this.baseUrl + 'employee/personal-data/',
        { userId: this.accountService.currentUser.id, employeePersonalData: personalData },
        { headers: requestAuthHeaders });

    return request.pipe(
      mergeMap(() => {
        return this.httpClient.get<IEmployee>
        (this.baseUrl + 'employee/' + this.accountService.currentUser.id, {headers: requestAuthHeaders});
      }))
      .pipe(mergeMap((data: IEmployee) => {
        this.currentEmployee$.next(data);
        return of([]);
      }));
  }

  getCurrentEmployee() {
    if (this.employeeInitialized) {
      return this.currentEmployee$;
    } else {
      const subscription = this.initializeCurrentlyLoggedEmployee()
        .subscribe((data: IEmployee) => {
          this.currentEmployee$.next(data);
        }, error => {
          this.employeeInitialized = false;
        }).add(() => subscription.unsubscribe());

      this.employeeInitialized = true;
      return this.currentEmployee$;
    }
  }

  getEmployeeProfileById(id: string) {
    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.accountService.getToken());
    return this.httpClient
      .get(this.baseUrl + 'employer/employee-profile/' + id, { headers: requestAuthHeaders });
  }

  private initializeCurrentlyLoggedEmployee() {
    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.accountService.getToken());
    return this.httpClient
      .get(this.baseUrl + 'employee/' + this.accountService.currentUser.id, { headers: requestAuthHeaders });
  }

  tryDeleteCertificateElement(certificateElementId: string) {
    this.tryDeleteCertificateElement$.next(certificateElementId);
  }

  tryDeleteEducationElement(educationElementId: string) {
    this.tryDeleteEducationElement$.next(educationElementId);
  }

  tryDeleteEmployeeSkill(skillId: string) {
    this.tryDeleteEmployeeSkill$.next(skillId);
  }

  tryDeleteEmployeeLanguage(languageId: string) {
    this.tryDeleteEmployeeLanguage$.next(languageId);
  }

  tryDeleteProfessionalExperienceElement(experienceElementId: string) {
    this.tryDeleteProfessionalExperienceElement$.next(experienceElementId);
  }

  private resetState() {
    this.currentEmployee$ = new ReplaySubject<IEmployee>(1);
    this.employeeInitialized = false;
  }

  ngOnDestroy(): void {
    this.accountServiceLogoutEventSubscription.unsubscribe();
  }
}
