import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeePublicProfileComponent } from './employee-public-profile.component';

describe('EmployeePublicProfileComponent', () => {
  let component: EmployeePublicProfileComponent;
  let fixture: ComponentFixture<EmployeePublicProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeePublicProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeePublicProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
