import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {EmployeeService} from '../employee.service';
import {ActivatedRoute} from '@angular/router';
import {polishApplicationGenderOptionsTranslator} from '../../shared/applicationGenders';
import {IEmployeeProfile} from '../../interfaces/IEmployeeProfile';

@Component({
  selector: 'app-employee-public-profile',
  templateUrl: './employee-public-profile.component.html',
  styleUrls: ['./employee-public-profile.component.css']
})
export class EmployeePublicProfileComponent implements OnInit, OnDestroy {
  polishApplicationGenderOptionsTranslator = polishApplicationGenderOptionsTranslator;

  aboutBaseShortenCharLimit = 600;
  aboutShortenCharLimit = 600;
  employeeData: IEmployeeProfile;

  // SUBSCRIPTIONS
  currentEmployeeSubscription: Subscription;

  constructor(private employeeService: EmployeeService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    const id = this.activatedRoute.snapshot.params['id'];
    this.currentEmployeeSubscription = this.employeeService.getEmployeeProfileById(id)
      .subscribe((employee: IEmployeeProfile) => {
      console.log(employee);
      this.employeeData = employee;
    });
  }

  isAboutCharLimitReached(): boolean {
    return this.employeeData.about.length > this.aboutBaseShortenCharLimit;
  }

  isInShowMoreMode(): boolean {
    return this.aboutShortenCharLimit !== this.aboutBaseShortenCharLimit;
  }

  showLess() {
    this.aboutShortenCharLimit = this.aboutBaseShortenCharLimit;
  }

  showMore() {
    this.aboutShortenCharLimit = this.employeeData.about.length;
  }

  ngOnDestroy(): void {
    this.currentEmployeeSubscription.unsubscribe();
  }
}
