import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {IEmployeeCertificate} from '../../../interfaces/IEmployeeCertificate';
import {EmployeeService} from '../../employee.service';

@Component({
  selector: 'app-certificates',
  templateUrl: './certificates.component.html',
  styleUrls: ['./certificates.component.css']
})
export class CertificatesComponent implements OnInit, OnDestroy {
  employeeCertificateAddMode = false;
  employeeCertificateEditMode = false;
  addingOrEditingEmployeeCertificateInProgress = false;
  deletingEmployeeCertificateInProgress = false;
  currentEditedEmployeeCertificateElementIndex = -1;
  currentDeletedEmployeeCertificateElementIndex = -1;

  employeeCertificates: Array<{ isInEditMode: boolean, employeeCertificate: IEmployeeCertificate }>;
  employeeCertificateForm: FormGroup;
  employeeCertificate: IEmployeeCertificate;

  // SUBSCRIPTIONS
  currentEmployeeSubscription: Subscription;
  confirmDeletionEmployeeCertificateSubscription: Subscription;

  constructor(private employeeService: EmployeeService) { }

  ngOnInit() {
    this.currentEmployeeSubscription = this.employeeService.getCurrentEmployee().subscribe(employee => {
      this.employeeCertificates = [];
      for (let i = 0; i < employee.employeeCertificates.length; i++) {
        this.employeeCertificates.push({isInEditMode: false, employeeCertificate: employee.employeeCertificates[i]});
      }
    });

    this.confirmDeletionEmployeeCertificateSubscription
      = this.employeeService.confirmDeletionCertificateElement$.subscribe((educationElementId) => {
          this.deletingEmployeeCertificateInProgress = true;
          this.employeeService.deleteCertificateElement(educationElementId)
            .subscribe().add(() => {
              this.deletingEmployeeCertificateInProgress = false;
              this.currentDeletedEmployeeCertificateElementIndex = -1;
          });
    });
  }

  submitChanges() {
    this.addingOrEditingEmployeeCertificateInProgress = true;
    this.assignFormValues();
    if (this.employeeCertificateAddMode) {
      this.employeeService.addEmployeeCertificate(this.employeeCertificate)
        .subscribe(() => {
            this.cancelAddingOrEditingEmployeeCertificate();
          },
          error => console.log(error))
        .add(() => this.addingOrEditingEmployeeCertificateInProgress = false);
    } else {
      this.employeeService.editEmployeeCertificate(this.employeeCertificate)
        .subscribe(() => {
          this.cancelAddingOrEditingEmployeeCertificate();
        })
        .add(() => this.addingOrEditingEmployeeCertificateInProgress = false);
    }
  }

  addEmployeeCertificate() {
    this.employeeCertificateEditMode = false;
    this.employeeCertificateAddMode = true;
    this.initializeNewForm();
  }

  cancelAddingOrEditingEmployeeCertificate() {
    this.employeeCertificateAddMode = false;
    this.employeeCertificateEditMode = false;
    this.currentEditedEmployeeCertificateElementIndex = -1;
    this.resetForm();
  }

  deleteEmployeeCertificateElement(elementIndex: number) {
    this.currentDeletedEmployeeCertificateElementIndex = elementIndex;
    this.employeeService.tryDeleteCertificateElement(this.employeeCertificates[elementIndex].employeeCertificate.courseId);
  }

  editEmployeeCertificateElement(elementIndex: number) {
    this.employeeCertificateAddMode = false;
    this.currentEditedEmployeeCertificateElementIndex = elementIndex;
    this.employeeCertificateEditMode = true;
    this.initializeEditForm(elementIndex);
  }

  isEmployeeCertificateElementEdited(elementIndex: number) {
    return this.currentEditedEmployeeCertificateElementIndex === elementIndex;
  }

  ngOnDestroy(): void {
    this.currentEmployeeSubscription.unsubscribe();
    this.confirmDeletionEmployeeCertificateSubscription.unsubscribe();
  }

  // Private
  private assignFormValues() {
    this.employeeCertificate = {
      courseId: this.employeeCertificateEditMode
        ? this.employeeCertificates[this.currentEditedEmployeeCertificateElementIndex].employeeCertificate.courseId
        : null,
      courseName: this.employeeCertificateForm.get('certificateName').value,
      organizerName: this.employeeCertificateForm.get('certificateOrganizerName').value,
      monthOfObtain: this.employeeCertificateForm.get('monthOfObtain').value,
      yearOfObtain: this.employeeCertificateForm.get('yearOfObtain').value,
    };
  }

  private initializeNewForm() {
    this.employeeCertificateForm = new FormGroup({
      'certificateName': new FormControl(null, Validators.required),
      'certificateOrganizerName': new FormControl(null, Validators.required),
      'monthOfObtain': new FormControl(null,
        [Validators.required ,  Validators.min(1), Validators.max(12)]),
      'yearOfObtain': new FormControl(null,
        [Validators.required, Validators.max(2018)]),
    });
  }

  private initializeEditForm(elementIndex: number) {
    this.employeeCertificateForm = new FormGroup({
      'certificateName': new FormControl(this.employeeCertificates[elementIndex].employeeCertificate.courseName, Validators.required),
      'certificateOrganizerName': new FormControl(this.employeeCertificates[elementIndex].employeeCertificate.organizerName,
        Validators.required),
      'monthOfObtain': new FormControl(this.employeeCertificates[elementIndex].employeeCertificate.monthOfObtain,
        [Validators.required , Validators.min(1), Validators.max(12)]),
      'yearOfObtain': new FormControl(this.employeeCertificates[elementIndex].employeeCertificate.yearOfObtain,
        [Validators.required, Validators.max(2018)]),
    });
  }

  private resetForm() {
    this.employeeCertificateForm.reset();
  }
}
