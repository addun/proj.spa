import {Component, OnDestroy, OnInit} from '@angular/core';
import {EmployeeService} from '../../employee.service';
import {Subscription} from 'rxjs';
import {IEmployeeLanguage} from '../../../interfaces/IEmployeeLanguage';
import {FormControl, FormGroup} from '@angular/forms';
import {AutocompleteService} from '../../../shared/services/autocomplete.service';
import {debounceTime, distinctUntilChanged, mergeMap} from 'rxjs/internal/operators';
import {IEditEmployeeLanguage} from '../../../interfaces/IEditEmployeeLanguage';

@Component({
  selector: 'app-languages',
  templateUrl: './languages.component.html',
  styleUrls: ['./languages.component.css']
})
export class LanguagesComponent implements OnInit, OnDestroy {
  employeeLanguageAddMode = false;
  employeeLanguageEditMode = false;
  addingEmployeeLanguageInProgress = false;
  deletingEmployeeLanguageInProgress = false;
  editingEmpoyeeLanguageInProgress = false;
  currentEditedEmployeeLanguageElementIndex = -1;

  autocompletionVisible = false;
  languageAutocompleteOptions: string[] = [];

  employeeLanguages: IEmployeeLanguage[] = [];
  addEmployeeLanguageForm: FormGroup;
  editEmployeeLanguageForm: FormGroup;
  employeeLanguage: IEmployeeLanguage;
  editEmployeeLanguage: IEditEmployeeLanguage;

  // SUBSCRIPTIONS
  currentEmployeeSubscription: Subscription;
  confirmDeletionEmployeeLanguageSubscription: Subscription;

  constructor(private employeeService: EmployeeService,
              private autocompleteService: AutocompleteService) { }

  ngOnInit() {
    this.initializeNewForm();

    this.currentEmployeeSubscription = this.employeeService.getCurrentEmployee().subscribe(employee => {
      this.employeeLanguages = employee.employeeLanguages;
    });

    this.confirmDeletionEmployeeLanguageSubscription
      = this.employeeService.confirmDeletionEmployeeLanguage$.subscribe((languageId) => {
      this.deletingEmployeeLanguageInProgress = true;
      this.employeeService.deleteEmployeeLanguage(languageId)
        .subscribe().add(() => this.deletingEmployeeLanguageInProgress = false);
    });
  }

  onClickAutocompleteOption(index: number) {
    this.addEmployeeLanguageForm.controls['languageName']
      .patchValue(this.languageAutocompleteOptions[index], {emitEvent: false});
    this.autocompletionVisible = false;
  }

  submitAddingEmployeeSkill() {
    this.addingEmployeeLanguageInProgress = true;
    this.assignFormValues();

    this.employeeService.addEmployeeLanguage(this.employeeLanguage)
      .subscribe(() => {
          this.cancelAddingEmployeeLanguage();
        },
        error => console.log(error))
      .add(() => this.addingEmployeeLanguageInProgress = false);
  }

  submitEditingEmployeeSkill() {
    this.editingEmpoyeeLanguageInProgress = true;
    this.editEmployeeLanguage = {
      'languageId': this.employeeLanguages[this.currentEditedEmployeeLanguageElementIndex].languageId,
      'level': this.editEmployeeLanguageForm.get('level').value,
    };

    this.employeeService.editEmployeeLanguage(this.editEmployeeLanguage)
      .subscribe(() => {
        this.cancelEditingEmployeeLanguage();
      })
      .add(() => this.editingEmpoyeeLanguageInProgress = false);
  }

  addEmployeeLanguage() {
    this.employeeLanguageEditMode = false;
    this.employeeLanguageAddMode = true;
    this.initializeNewForm();
  }

  cancelAddingEmployeeLanguage() {
    this.employeeLanguageAddMode = false;
    this.resetAddLanguageForm();
  }

  cancelEditingEmployeeLanguage() {
    this.employeeLanguageEditMode = false;
    this.currentEditedEmployeeLanguageElementIndex = -1;
    this.resetEditLanguageForm();
  }

  deleteEmployeeLanguageElement(elementIndex: number) {
    this.employeeService.tryDeleteEmployeeLanguage(this.employeeLanguages[elementIndex].languageId);
  }

  editEmployeeLanguageElement(elementIndex: number) {
    this.employeeLanguageAddMode = false;
    this.currentEditedEmployeeLanguageElementIndex = elementIndex;
    this.employeeLanguageEditMode = true;
    this.initializeEditForm(elementIndex);
  }

  ngOnDestroy(): void {
    this.currentEmployeeSubscription.unsubscribe();
    this.confirmDeletionEmployeeLanguageSubscription.unsubscribe();
  }

  // PRIVATE
  private assignFormValues() {
    this.employeeLanguage = {
      'languageId': null,
      'languageName': this.addEmployeeLanguageForm.get('languageName').value,
      'level': this.addEmployeeLanguageForm.get('level').value,
    };
  }

  private initializeAutocompletion() {
    // AUTOCOMPLETE SET UP
    this.addEmployeeLanguageForm.controls['languageName'].valueChanges
      .pipe(debounceTime(500), distinctUntilChanged())
      .pipe(mergeMap((data) => {
        if (data === '' || data === null) {
          this.autocompletionVisible = false;
          return [];
        }
        return this.autocompleteService.autocompleteLanguages(data);
      }))
      .subscribe((data) => {
        this.languageAutocompleteOptions = data;
        if (this.languageAutocompleteOptions.length > 0) {
          this.autocompletionVisible = true;
        } else {
          this.autocompletionVisible = false;
        }
      });
  }

  private initializeNewForm() {
    this.addEmployeeLanguageForm = new FormGroup({
      'languageName': new FormControl(null),
      'level': new FormControl(1),
    });
    this.initializeAutocompletion();
  }

  private initializeEditForm(elementIndex: number) {
    this.editEmployeeLanguageForm = new FormGroup({
      'readonlyLanguageName': new FormControl(this.employeeLanguages[elementIndex].languageName),
      'level': new FormControl(this.employeeLanguages[elementIndex].level),
    });
  }

  private resetAddLanguageForm() {
    this.addEmployeeLanguageForm.reset({'level': 1});
  }

  private resetEditLanguageForm() {
    this.editEmployeeLanguageForm.reset();
  }
}
