import {Component, OnDestroy, OnInit} from '@angular/core';
import {IEmployee} from '../../../interfaces/IEmployee';
import {EmployeeService} from '../../employee.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {IEmployeePersonalData} from '../../../interfaces/IEmployeePersonalData';
import {Subscription} from 'rxjs';
import {applicationGenderOptions, polishApplicationGenderOptionsTranslator} from '../../../shared/applicationGenders';

@Component({
  selector: 'app-edit-personal-data',
  templateUrl: './personal-data.component.html',
  styleUrls: ['./personal-data.component.css']
})
export class PersonalDataComponent implements OnInit, OnDestroy {
  applicationGenderOptions = applicationGenderOptions;
  polishApplicationGenderOptionsTranslator = polishApplicationGenderOptionsTranslator;
  employeeData: IEmployee;
  employeePersonalData: IEmployeePersonalData;
  personalDataEditMode = false;
  editPersonalDataForm: FormGroup;
  changingPersonalDataInProgress = false;

  currentEmployeeSubscription: Subscription;

  constructor(private employeeService: EmployeeService) { }

  ngOnInit() {
    this.currentEmployeeSubscription = this.employeeService.getCurrentEmployee()
        .subscribe(employee => this.employeeData = employee);
  }

  cancelEditPersonalData() {
    this.personalDataEditMode = false;
    this.resetForm();
  }

  editPersonalData() {
    this.personalDataEditMode = true;
    this.initializeForm();
  }

  submitPersonalData() {
    this.changingPersonalDataInProgress = true;
    this.assignFormValues();
    this.employeeService.editPersonalData(this.employeePersonalData)
      .subscribe(() => this.personalDataEditMode = false,
      (errorResponse) => {
        const errors = errorResponse.error;
        for (let key in errors) {
          this.editPersonalDataForm.get(key).setErrors({'serverError' : errors[key]});
        }
      })
      .add(() => this.changingPersonalDataInProgress =  false);
  }

  ngOnDestroy(): void {
    this.currentEmployeeSubscription.unsubscribe();
  }

  private assignFormValues() {
    this.employeePersonalData = {
      email: this.editPersonalDataForm.get('email').value,
      name: this.editPersonalDataForm.get('name').value,
      surname: this.editPersonalDataForm.get('surname').value,
      birthDate: this.editPersonalDataForm.get('birthDate').value,
      sex: this.editPersonalDataForm.get('sex').value,
      country: this.editPersonalDataForm.get('country').value,
      city: this.editPersonalDataForm.get('city').value,
      phone: this.editPersonalDataForm.get('phone').value,
      websiteUrl: this.editPersonalDataForm.get('websiteUrl').value,
      about: this.editPersonalDataForm.get('about').value
    };
  }

  private initializeForm() {
    this.editPersonalDataForm = new FormGroup({
      'name': new FormControl(this.employeeData.name, Validators.required),
      'surname': new FormControl(this.employeeData.surname, Validators.required),
      'email': new FormControl(this.employeeData.email, [Validators.required, Validators.email]),
      'about': new FormControl(this.employeeData.about),
      'birthDate': new FormControl(this.employeeData.birthDate, Validators.required),
      'sex': new FormControl(this.employeeData.sex),
      'phone': new FormControl(this.employeeData.phone),
      'country': new FormControl(this.employeeData.country, Validators.required),
      'city': new FormControl(this.employeeData.city, Validators.required),
      'websiteUrl': new FormControl(this.employeeData.websiteUrl),
    });
  }

  private resetForm() {
    this.editPersonalDataForm.reset();
  }
}
