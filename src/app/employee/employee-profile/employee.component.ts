import { Component, OnDestroy, OnInit} from '@angular/core';

import { IEmployee } from '../../interfaces/IEmployee';
import {EmployeeService} from '../employee.service';
import {Subscription} from 'rxjs';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ModalComponent} from '../../shared/components/modal/modal.component';


@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit, OnDestroy {
  aboutBaseShortenCharLimit = 600;
  aboutShortenCharLimit = 600;
  hasEmailConfirmed = true;
  employeeData: IEmployee;

  // SUBSCRIPTIONS
  currentEmployeeSubscription: Subscription;
  tryDeleteCertificateElementSubscription: Subscription;
  tryDeleteEducationElementSubscription: Subscription;
  tryDeleteEmployeeLanguageSubscription: Subscription;
  tryDeleteEmployeeSkillSubscription: Subscription;
  tryDeleteProfessionalExperienceElement: Subscription;

  constructor(private modalService: NgbModal, private employeeService: EmployeeService) { }

  ngOnInit() {
    this.currentEmployeeSubscription = this.employeeService.getCurrentEmployee().subscribe(employee => {
      console.log(employee);
      this.employeeData = employee;
      this.hasEmailConfirmed = employee.emailConfirmed;
    });

    this.tryDeleteCertificateElementSubscription
      = this.employeeService.tryDeleteCertificateElement$.subscribe((certificateElementId) => {
      const modalRef = this.modalService.open(ModalComponent);
      modalRef.componentInstance.message = 'Czy na pewno chcesz usunąć certifikat?';
      modalRef.componentInstance.closeButtonTitle = 'Usuń';
      modalRef.componentInstance.dismissButtonTitle = 'Anuluj';

      modalRef.result.then(() => {
        this.employeeService.confirmDeletionCertificateElement(certificateElementId);
      }, () => {});
    });

    this.tryDeleteEducationElementSubscription
      = this.employeeService.tryDeleteEducationElement$.subscribe((educationElementId) => {
      const modalRef = this.modalService.open(ModalComponent);
      modalRef.componentInstance.message = 'Czy na pewno chcesz usunąć element historii edukacji?';
      modalRef.componentInstance.closeButtonTitle = 'Usuń';
      modalRef.componentInstance.dismissButtonTitle = 'Anuluj';

      modalRef.result.then(() => {
        this.employeeService.confirmDeletionEducationElement(educationElementId);
      }, () => {});
    });

    this.tryDeleteEmployeeLanguageSubscription
      = this.employeeService.tryDeleteEmployeeLanguage$.subscribe((languageId) => {
      const modalRef = this.modalService.open(ModalComponent);
      modalRef.componentInstance.message = 'Czy na pewno chcesz usunąć język?';
      modalRef.componentInstance.closeButtonTitle = 'Usuń';
      modalRef.componentInstance.dismissButtonTitle = 'Anuluj';

      modalRef.result.then(() => {
        this.employeeService.confirmDeletionEmployeeLanguage(languageId);
      }, () => {});
    });

    this.tryDeleteEmployeeSkillSubscription
      = this.employeeService.tryDeleteEmployeeSkill$.subscribe((skillId) => {
      const modalRef = this.modalService.open(ModalComponent);
      modalRef.componentInstance.message = 'Czy na pewno chcesz usunąć tę umiejętność?';
      modalRef.componentInstance.closeButtonTitle = 'Usuń';
      modalRef.componentInstance.dismissButtonTitle = 'Anuluj';

      modalRef.result.then(() => {
        this.employeeService.confirmDeletionEmployeeSkill(skillId);
      }, () => {});
    });

    this.tryDeleteProfessionalExperienceElement
      = this.employeeService.tryDeleteProfessionalExperienceElement$.subscribe((professionalExperienceElementId) => {
      const modalRef = this.modalService.open(ModalComponent);
      modalRef.componentInstance.message = 'Czy na pewno chcesz usunąć element?';
      modalRef.componentInstance.closeButtonTitle = 'Usuń';
      modalRef.componentInstance.dismissButtonTitle = 'Anuluj';

      modalRef.result.then(() => {
        this.employeeService.confirmDeletionProfessionalExperienceElement(professionalExperienceElementId);
      }, () => {});
    });
  }

  isAboutCharLimitReached(): boolean {
    return this.employeeData.about.length > this.aboutBaseShortenCharLimit;
  }

  isInShowMoreMode(): boolean {
    return this.aboutShortenCharLimit !== this.aboutBaseShortenCharLimit;
  }

  showLess() {
    this.aboutShortenCharLimit = this.aboutBaseShortenCharLimit;
  }

  showMore() {
    this.aboutShortenCharLimit = this.employeeData.about.length;
  }

  ngOnDestroy(): void {
    this.currentEmployeeSubscription.unsubscribe();
    this.tryDeleteCertificateElementSubscription.unsubscribe();
    this.tryDeleteEducationElementSubscription.unsubscribe();
    this.tryDeleteProfessionalExperienceElement.unsubscribe();
    this.tryDeleteEmployeeLanguageSubscription.unsubscribe();
    this.tryDeleteEmployeeSkillSubscription.unsubscribe();
  }
}
