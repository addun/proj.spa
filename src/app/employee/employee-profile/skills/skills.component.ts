import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {EmployeeService} from '../../employee.service';
import {IEmployeeSkill} from '../../../interfaces/IEmployeeSkill';
import {FormControl, FormGroup} from '@angular/forms';
import {debounceTime, distinctUntilChanged, mergeMap} from 'rxjs/internal/operators';
import {AutocompleteService} from '../../../shared/services/autocomplete.service';
import {IEditEmployeeSkill} from '../../../interfaces/IEditEmployeeSkill';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.css']
})
export class SkillsComponent implements OnInit, OnDestroy {
  employeeSkillAddMode = false;
  employeeSkillEditMode = false;
  addingEmployeeSkillInProgress = false;
  deletingEmployeeSkillInProgress = false;
  editingEmpoyeeSkillInProgress = false;
  currentEditedEmployeeSkillElementIndex = -1;

  autocompletionVisible = false;
  skillsAutocompleteOptions: string[] = [];

  employeeSkills: IEmployeeSkill[] = [];
  addEmployeeSkillForm: FormGroup;
  editEmployeeSkillForm: FormGroup;
  employeeSkill: IEmployeeSkill;
  editEmployeeSkill: IEditEmployeeSkill;

  // SUBSCRIPTIONS
  currentEmployeeSubscription: Subscription;
  confirmDeletionEmployeeSkillSubscription: Subscription;

  constructor(private employeeService: EmployeeService,
              private autocompleteService: AutocompleteService) { }

  ngOnInit() {
    this.initializeNewForm();

    this.currentEmployeeSubscription = this.employeeService.getCurrentEmployee().subscribe(employee => {
      this.employeeSkills = employee.employeeSkills;
    });

    this.confirmDeletionEmployeeSkillSubscription
      = this.employeeService.confirmDeletionEmployeeSkill$.subscribe((skillId) => {
      this.deletingEmployeeSkillInProgress = true;
      this.employeeService.deleteEmployeeSkill(skillId)
        .subscribe().add(() => this.deletingEmployeeSkillInProgress = false);
    });
  }

  onClickAutocompleteOption(index: number) {
    this.addEmployeeSkillForm.controls.skillName
      .patchValue(this.skillsAutocompleteOptions[index], {emitEvent: false});
    this.autocompletionVisible = false;
  }

  submitAddingEmployeeSkill() {
    this.addingEmployeeSkillInProgress = true;
    this.assignFormValues();

    this.employeeService.addEmployeeSkill(this.employeeSkill)
      .subscribe(() => {
          this.cancelAddingEmployeeSkill();
        },
        error => console.log(error))
      .add(() => this.addingEmployeeSkillInProgress = false);
  }

  submitEditingEmployeeSkill() {
    this.editingEmpoyeeSkillInProgress = true;
    this.editEmployeeSkill = {
      'skillId': this.employeeSkills[this.currentEditedEmployeeSkillElementIndex].skillId,
      'level': this.editEmployeeSkillForm.get('level').value,
    };

    this.employeeService.editEmployeeSkill(this.editEmployeeSkill)
      .subscribe(() => {
        this.cancelEditingEmployeeSkill();
      })
      .add(() => this.editingEmpoyeeSkillInProgress = false);
  }

  addEmployeeSkill() {
    this.employeeSkillEditMode = false;
    this.employeeSkillAddMode = true;
    this.initializeNewForm();
  }

  cancelAddingEmployeeSkill() {
    this.employeeSkillAddMode = false;
    this.resetAddSkillForm();
  }

  cancelEditingEmployeeSkill() {
    this.employeeSkillEditMode = false;
    this.currentEditedEmployeeSkillElementIndex = -1;
    this.resetEditSkillForm();
  }

  deleteEmployeeSkillElement(elementIndex: number) {
    this.employeeService.tryDeleteEmployeeSkill(this.employeeSkills[elementIndex].skillId);
  }

  editEmployeeSkillElement(elementIndex: number) {
    this.employeeSkillAddMode = false;
    this.currentEditedEmployeeSkillElementIndex = elementIndex;
    this.employeeSkillEditMode = true;
    this.initializeEditForm(elementIndex);
  }

  ngOnDestroy(): void {
    this.currentEmployeeSubscription.unsubscribe();
    this.confirmDeletionEmployeeSkillSubscription.unsubscribe();
  }

  // PRIVATE
  private assignFormValues() {
    this.employeeSkill = {
      'skillId': null,
      'skillName': this.addEmployeeSkillForm.get('skillName').value,
      'level': this.addEmployeeSkillForm.get('level').value,
    };
  }

  private initializeAutocompletion() {
    // AUTOCOMPLETE SET UP
    this.addEmployeeSkillForm.controls.skillName.valueChanges
      .pipe(debounceTime(500), distinctUntilChanged())
      .pipe(mergeMap((data) => {
        if (data === '' || data === null) {
          this.autocompletionVisible = false;
          return [];
        }
        return this.autocompleteService.autocompleteSkills(data);
      }))
      .subscribe((data) => {
        this.skillsAutocompleteOptions = data;
        if (this.skillsAutocompleteOptions.length > 0) {
          this.autocompletionVisible = true;
        } else {
          this.autocompletionVisible = false;
        }
      });
  }

  private initializeNewForm() {
    this.addEmployeeSkillForm = new FormGroup({
        skillName: new FormControl(null),
        level: new FormControl(1),
    });
    this.initializeAutocompletion();
  }

  private initializeEditForm(elementIndex: number) {
    this.editEmployeeSkillForm = new FormGroup({
      readonlySkillName: new FormControl(this.employeeSkills[elementIndex].skillName),
      level: new FormControl(this.employeeSkills[elementIndex].level),
    });
  }

  private resetAddSkillForm() {
    this.addEmployeeSkillForm.reset({'level': 1});
  }

  private resetEditSkillForm() {
    this.editEmployeeSkillForm.reset();
  }
}
