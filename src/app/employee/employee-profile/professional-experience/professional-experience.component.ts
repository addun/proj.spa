import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {IEmployee} from '../../../interfaces/IEmployee';
import {EmployeeService} from '../../employee.service';
import {IEmployeeJobExperience} from '../../../interfaces/IEmployeeJobExperience';
import {Subscription} from 'rxjs';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {forEach} from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-professional-experience',
  templateUrl: './professional-experience.component.html',
  styleUrls: ['./professional-experience.component.css']
})
export class ProfessionalExperienceComponent implements OnInit, OnDestroy {
  professionalExperienceAddMode = false;
  professionalExperienceEditMode = false;
  addingOrEditingEmployeeExperienceInProgress = false;
  deletingEmployeeExperienceInProgress = false;
  currentEditedProfessionalExperienceElementIndex = -1;
  currentDeletedProfessionalExperienceElementIndex = -1;

  employeeJobForm: FormGroup;
  employeeProfessionalExperienceFormCheckboxValue = false;
  employeeJobs: Array<{ isInEditMode: boolean, employeeJob: IEmployeeJobExperience }>;
  employeeJob: IEmployeeJobExperience;

  // SUBSCRIPTIONS
  currentEmployeeSubscription: Subscription;
  confirmDeletionEmployeeProfessionalExperienceSubscription: Subscription;

  constructor(private employeeService: EmployeeService) { }

  ngOnInit() {
    this.currentEmployeeSubscription = this.employeeService.getCurrentEmployee().subscribe(employee => {
      this.employeeJobs = [];
      for (let i = 0; i < employee.jobExperience.length; i++) {
        this.employeeJobs.push({isInEditMode: false, employeeJob: employee.jobExperience[i]});
      }
    });

    this.confirmDeletionEmployeeProfessionalExperienceSubscription
        = this.employeeService.confirmDeletionProfessionalExperienceElement$.subscribe((professionalExperienceElementId) => {
      this.deletingEmployeeExperienceInProgress = true;
      this.employeeService.deleteProfessionalExperienceElement(professionalExperienceElementId)
        .subscribe().add(() => {
          this.deletingEmployeeExperienceInProgress = false;
          this.currentDeletedProfessionalExperienceElementIndex = -1;
      });
    });
  }

  submitChanges() {
    this.addingOrEditingEmployeeExperienceInProgress = true;
    this.assignFormValues();
    if (this.professionalExperienceAddMode) {
      this.employeeService.addEmployeeProfessionalExperience(this.employeeJob)
        .subscribe(() => {
          this.cancelAddingOrEditingProfessionalExperience();
        },
        error => console.log(error))
        .add(() => this.addingOrEditingEmployeeExperienceInProgress = false);
    } else {
      this.employeeService.editEmployeeProfessionalExperience(this.employeeJob)
        .subscribe(() => {
          this.cancelAddingOrEditingProfessionalExperience();
        })
        .add(() => this.addingOrEditingEmployeeExperienceInProgress = false);
    }
  }

  addProfessionalExperience() {
    this.professionalExperienceEditMode = false;
    this.professionalExperienceAddMode = true;
    this.initializeNewForm();
  }

  cancelAddingOrEditingProfessionalExperience() {
    this.professionalExperienceAddMode = false;
    this.professionalExperienceEditMode = false;
    this.currentEditedProfessionalExperienceElementIndex = -1;
    this.resetForm();
  }

  deleteProfessionalExperienceElement(elementIndex: number) {
    this.currentDeletedProfessionalExperienceElementIndex = elementIndex;
    this.employeeService.tryDeleteProfessionalExperienceElement(this.employeeJobs[elementIndex].employeeJob.professionalExperienceId);
  }

  editProfessionalExperienceElement(elementIndex: number) {
    this.professionalExperienceAddMode = false;
    this.currentEditedProfessionalExperienceElementIndex = elementIndex;
    this.professionalExperienceEditMode = true;
    this.initializeEditForm(elementIndex);
  }

  isProfessionalExperienceElementEdited(elementIndex: number) {
    return this.currentEditedProfessionalExperienceElementIndex === elementIndex;
  }

  ngOnDestroy(): void {
    this.currentEmployeeSubscription.unsubscribe();
    this.confirmDeletionEmployeeProfessionalExperienceSubscription.unsubscribe();
  }

  toggleFormCheckbox($event: Event) {
    if ((<HTMLInputElement>$event.target).checked) {
      this.employeeJobForm.get('endMonth').patchValue(null);
      this.employeeJobForm.get('endYear').patchValue(null);
      this.employeeJobForm.get('endMonth').disable();
      this.employeeJobForm.get('endYear').disable();
    } else {
      this.employeeJobForm.get('endMonth').enable();
      this.employeeJobForm.get('endYear').enable();
    }
  }

  // Private
  private assignFormValues() {
    this.employeeJob = {
      professionalExperienceId: this.professionalExperienceEditMode
        ? this.employeeJobs[this.currentEditedProfessionalExperienceElementIndex].employeeJob.professionalExperienceId
        : null,
      position: this.employeeJobForm.get('position').value,
      companyName: this.employeeJobForm.get('companyName').value,
      country: this.employeeJobForm.get('country').value,
      city: this.employeeJobForm.get('city').value,
      startMonth: this.employeeJobForm.get('startMonth').value,
      startYear: this.employeeJobForm.get('startYear').value,
      endMonth: this.employeeJobForm.get('endMonth').value,
      endYear: this.employeeJobForm.get('endYear').value,
      jobDescription: this.employeeJobForm.get('jobDescription').value
    };
  }

  private initializeNewForm() {
    this.employeeJobForm = new FormGroup({
      'position': new FormControl(null, Validators.required),
      'country': new FormControl(null, Validators.required),
      'city': new FormControl(null, Validators.required),
      'companyName': new FormControl(null, Validators.required),
      'startMonth': new FormControl(null,
        [Validators.required, Validators.min(1), Validators.max(12)]),
      'startYear': new FormControl(null,
        [Validators.required, Validators.min(1950), Validators.max(2018)]),
      'endMonth': new FormControl(null,
        [Validators.min(1), Validators.max(12)]),
      'endYear': new FormControl(null,
        [Validators.min(1950), Validators.max(2018)]),
      'jobDescription': new FormControl(null),
    });

    this.employeeProfessionalExperienceFormCheckboxValue = false;
  }

  private initializeEditForm(elementIndex: number) {
    this.employeeJobForm = new FormGroup({
      'position': new FormControl(this.employeeJobs[elementIndex].employeeJob.position, Validators.required),
      'country': new FormControl(this.employeeJobs[elementIndex].employeeJob.country, Validators.required),
      'city': new FormControl(this.employeeJobs[elementIndex].employeeJob.city, Validators.required),
      'companyName': new FormControl(this.employeeJobs[elementIndex].employeeJob.companyName, Validators.required),
      'startMonth': new FormControl(this.employeeJobs[elementIndex].employeeJob.startMonth,
        [Validators.required, Validators.min(1), Validators.max(12)]),
      'startYear': new FormControl(this.employeeJobs[elementIndex].employeeJob.startYear,
        [Validators.required, Validators.min(1950), Validators.max(2018)]),
      'endMonth': new FormControl(this.employeeJobs[elementIndex].employeeJob.endMonth,
        [Validators.min(1), Validators.max(12)]),
      'endYear': new FormControl(this.employeeJobs[elementIndex].employeeJob.endYear,
        [ Validators.min(1950), Validators.max(2018)]),
      'jobDescription': new FormControl(this.employeeJobs[elementIndex].employeeJob.jobDescription),
    });

    if (this.employeeJobs[elementIndex].employeeJob.endMonth === null && this.employeeJobs[elementIndex].employeeJob.endYear === null) {
      this.employeeProfessionalExperienceFormCheckboxValue = true;
      this.employeeJobForm.get('endMonth').disable();
      this.employeeJobForm.get('endYear').disable();
    }
  }

  private resetForm() {
    this.employeeJobForm.reset();
    this.employeeProfessionalExperienceFormCheckboxValue = false;
  }
}
