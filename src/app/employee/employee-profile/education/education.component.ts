import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import {IEmployeeEducation} from '../../../interfaces/IEmployeeEducation';
import {EmployeeService} from '../../employee.service';

@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.css']
})
export class EducationComponent implements OnInit, OnDestroy {
  employeeEducationAddMode = false;
  employeeEducationEditMode = false;
  addingOrEditingEmployeeEducationInProgress = false;
  deletingEmployeeEducationInProgress = false;
  currentEditedEmployeeEducationElementIndex = -1;
  currentDeletedEmployeeEducationElementIndex = -1;

  employeeEducationHistory: Array<{ isInEditMode: boolean, employeeEducation: IEmployeeEducation }>;
  employeeEducationForm: FormGroup;
  employeeEducationFormCheckboxValue = false;
  employeeEducation: IEmployeeEducation;

  // SUBSCRIPTIONS
  currentEmployeeSubscription: Subscription;
  confirmDeletionEmployeeEducationSubscription: Subscription;

  constructor(private employeeService: EmployeeService) { }

  ngOnInit() {
    this.currentEmployeeSubscription = this.employeeService.getCurrentEmployee().subscribe(employee => {
      this.employeeEducationHistory = [];
      for (let i = 0; i < employee.employeeEducation.length; i++) {
        this.employeeEducationHistory.push({isInEditMode: false, employeeEducation: employee.employeeEducation[i]});
      }
    });

    this.confirmDeletionEmployeeEducationSubscription
      = this.employeeService.confirmDeletionEducationElement$.subscribe((educationElementId) => {
      this.deletingEmployeeEducationInProgress = true;
      this.employeeService.deleteEducationElement(educationElementId)
        .subscribe().add(() => {
          this.deletingEmployeeEducationInProgress = false;
          this.currentDeletedEmployeeEducationElementIndex = -1;
      });
    });
  }

  submitChanges() {
    this.addingOrEditingEmployeeEducationInProgress = true;
    this.assignFormValues();
    if (this.employeeEducationAddMode) {
      this.employeeService.addEmployeeEducation(this.employeeEducation)
        .subscribe(() => {
            this.cancelAddingOrEditingEmployeeEducation();
          },
          error => console.log(error))
        .add(() => this.addingOrEditingEmployeeEducationInProgress = false);
    } else {
      this.employeeService.editEmployeeEducation(this.employeeEducation)
        .subscribe(() => {
          this.cancelAddingOrEditingEmployeeEducation();
        })
        .add(() => this.addingOrEditingEmployeeEducationInProgress = false);
    }
  }

  addEmployeeEducation() {
    this.employeeEducationEditMode = false;
    this.employeeEducationAddMode = true;
    this.initializeNewForm();
  }

  cancelAddingOrEditingEmployeeEducation() {
    this.employeeEducationAddMode = false;
    this.employeeEducationEditMode = false;
    this.currentEditedEmployeeEducationElementIndex = -1;
    this.resetForm();
  }

  deleteEmployeeEducationElement(elementIndex: number) {
    this.currentDeletedEmployeeEducationElementIndex = elementIndex;
    this.employeeService.tryDeleteEducationElement(this.employeeEducationHistory[elementIndex].employeeEducation.educationId);
  }

  editEmployeeEducationElement(elementIndex: number) {
    this.employeeEducationAddMode = false;
    this.currentEditedEmployeeEducationElementIndex = elementIndex;
    this.employeeEducationEditMode = true;
    this.initializeEditForm(elementIndex);
  }

  isEmployeeEducationElementEdited(elementIndex: number) {
    return this.currentEditedEmployeeEducationElementIndex === elementIndex;
  }

  ngOnDestroy(): void {
    this.currentEmployeeSubscription.unsubscribe();
    this.confirmDeletionEmployeeEducationSubscription.unsubscribe();
  }

  toggleFormCheckbox($event: Event) {
    if ((<HTMLInputElement>$event.target).checked) {
      this.employeeEducationForm.get('endYear').patchValue(null);
      this.employeeEducationForm.get('endYear').disable();
    } else {
      this.employeeEducationForm.get('endYear').enable();
    }
  }

  // Private
  private assignFormValues() {
    this.employeeEducation = {
      educationId: this.employeeEducationEditMode
        ? this.employeeEducationHistory[this.currentEditedEmployeeEducationElementIndex].employeeEducation.educationId
        : null,
      schoolName: this.employeeEducationForm.get('schoolName').value,
      branchOfStudy: this.employeeEducationForm.get('branchOfStudy').value,
      speciality: this.employeeEducationForm.get('speciality').value,
      degree: this.employeeEducationForm.get('degree').value,
      startYear: this.employeeEducationForm.get('startYear').value,
      endYear: this.employeeEducationForm.get('endYear').value,
    };
  }

  private initializeNewForm() {
    this.employeeEducationForm = new FormGroup({
      'schoolName': new FormControl(null, Validators.required),
      'branchOfStudy': new FormControl(null, Validators.required),
      'speciality': new FormControl(null),
      'degree': new FormControl(null, Validators.required),
      'startYear': new FormControl(null,
        [Validators.required, Validators.max(2018)]),
      'endYear': new FormControl(null, [Validators.max(2018)]),
    });

    this.employeeEducationFormCheckboxValue = false;
  }

  private initializeEditForm(elementIndex: number) {
    this.employeeEducationForm = new FormGroup({
      'schoolName': new FormControl(this.employeeEducationHistory[elementIndex].employeeEducation.schoolName, Validators.required),
      'branchOfStudy': new FormControl(this.employeeEducationHistory[elementIndex].employeeEducation.branchOfStudy, Validators.required),
      'speciality': new FormControl(this.employeeEducationHistory[elementIndex].employeeEducation.speciality),
      'degree': new FormControl(this.employeeEducationHistory[elementIndex].employeeEducation.degree, Validators.required),
      'startYear': new FormControl(this.employeeEducationHistory[elementIndex].employeeEducation.startYear,
        [Validators.required, Validators.max(2018)]),
      'endYear': new FormControl(this.employeeEducationHistory[elementIndex].employeeEducation.endYear,
        [Validators.max(2018)]),
    });

    // Disable endYear control if has no value
    if (this.employeeEducationHistory[elementIndex].employeeEducation.endYear === null) {
      this.employeeEducationFormCheckboxValue = true;
      this.employeeEducationForm.get('endYear').disable();
    }
  }

  private resetForm() {
    this.employeeEducationForm.reset();
    this.employeeEducationFormCheckboxValue = false;
  }
}
