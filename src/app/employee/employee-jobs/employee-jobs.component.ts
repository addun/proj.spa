import { Component, OnInit } from '@angular/core';
import {IEmployeeJobOffer} from '../../interfaces/IEmployeeJobOffer';
import {EmployeeService} from '../employee.service';

@Component({
  selector: 'app-employee-jobs',
  templateUrl: './employee-jobs.component.html',
  styleUrls: ['./employee-jobs.component.css']
})
export class EmployeeJobsComponent implements OnInit {
  jobOffersThatUserApplyFor: IEmployeeJobOffer[];
  favouriteJobOffers: IEmployeeJobOffer[];

  constructor(private employeeService: EmployeeService) { }

  ngOnInit() {
    this.employeeService.getCurrentEmployee()
      .subscribe(employee => {
        this.jobOffersThatUserApplyFor = employee.applications;
      });
  }

}
