import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {EmployeeComponent} from './employee-profile/employee.component';
import {AccountModule} from '../account/account.module';
import { PersonalDataComponent } from './employee-profile/personal-data/personal-data.component';
import { ProfessionalExperienceComponent } from './employee-profile/professional-experience/professional-experience.component';
import { EducationComponent } from './employee-profile/education/education.component';
import { CertificatesComponent } from './employee-profile/certificates/certificates.component';
import { ModalComponent } from '../shared/components/modal/modal.component';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SkillsComponent } from './employee-profile/skills/skills.component';
import { LanguagesComponent } from './employee-profile/languages/languages.component';
import {SharedModule} from '../shared/shared.module';
import { EmployeePublicProfileComponent } from './employee-public-profile/employee-public-profile.component';
import { EmployeeJobsComponent } from './employee-jobs/employee-jobs.component';
import { EmployeeFavouriteJobsComponent } from './employee-favourite-jobs/employee-favourite-jobs.component';

@NgModule({
  imports: [
    AccountModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  declarations: [
    EmployeeComponent,
    PersonalDataComponent,
    ProfessionalExperienceComponent,
    EducationComponent,
    CertificatesComponent,
    SkillsComponent,
    LanguagesComponent,
    EmployeePublicProfileComponent,
    EmployeeJobsComponent,
    EmployeeFavouriteJobsComponent,
  ],
  providers: [
    NgbActiveModal
  ],
  entryComponents: [
    ModalComponent
  ]
})
export class EmployeeModule { }
