import { Component, OnInit } from '@angular/core';
import {IEmployeeJobOffer} from '../../interfaces/IEmployeeJobOffer';
import {EmployeeService} from '../employee.service';

@Component({
  selector: 'app-employee-favourite-jobs',
  templateUrl: './employee-favourite-jobs.component.html',
  styleUrls: ['./employee-favourite-jobs.component.css']
})
export class EmployeeFavouriteJobsComponent implements OnInit {
  favouriteJobOffers: IEmployeeJobOffer[];

  constructor(private employeeService: EmployeeService) { }

  ngOnInit() {
    this.employeeService.getCurrentEmployee()
      .subscribe(employee => {
        this.favouriteJobOffers = employee.favourites;
      });
  }
}
