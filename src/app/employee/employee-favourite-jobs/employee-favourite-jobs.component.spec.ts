import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeFavouriteJobsComponent } from './employee-favourite-jobs.component';

describe('EmployeeFavouriteJobsComponent', () => {
  let component: EmployeeFavouriteJobsComponent;
  let fixture: ComponentFixture<EmployeeFavouriteJobsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeFavouriteJobsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeFavouriteJobsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
