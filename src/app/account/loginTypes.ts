export class LoginTypes {
    public static readonly employee: string = 'employee';
    public static readonly employer: string = 'employer';
}
