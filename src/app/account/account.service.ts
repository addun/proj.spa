import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {of, Subject} from 'rxjs';

import { IRegisterEmployer } from '../interfaces/IRegisterEmployer';
import { IRegisterEmployee } from '../interfaces/IRegisterEmployee';
import { ICredentials } from '../interfaces/ICredentials';
import { IResetPassword } from '../interfaces/IResetPassword';
import { IConfirmEmailDto } from '../interfaces/IConfirmEmail';
import { IForgotPassword } from '../interfaces/IForgotPassword';
import { LoginTypes } from './loginTypes';
import { mergeMap } from 'rxjs/internal/operators';
import {Router} from '@angular/router';


@Injectable()
export class AccountService {
  private tokenKey = 'authToken';
  private refreshTokenKey = 'refreshToken';
  private baseUrl = 'http://localhost:5000/api/';
  // private baseUrl = 'https://justfindit-api.azurewebsites.net/api/';
  private headers: HttpHeaders;

  userLogout$: Subject<null> = new Subject<null>();

  currentUser: { id: string, email: string, userType: string, rememberMe: boolean } = {
    id: null,
    email: null,
    userType: null,
    rememberMe: false,
  };

  registrationServerErrorOccured$: Subject<{ key: string, errorMessages: string[] }> = new Subject();
  loginServerErrorOccured$: Subject<{ key: string, errorMessages: string[] }> = new Subject();

  constructor(private httpClient: HttpClient, private router: Router) {
    if (this.getToken() !== null) {
      this.decodeJwtDataAndPopulateCurrentUser(this.getToken());
    }
    this.headers = new HttpHeaders();
  }

  confirmEmail(confirmEmailDto: IConfirmEmailDto) {
    return this.httpClient
      .post(this.baseUrl + 'account/confirm-email', confirmEmailDto);
  }

  forgotPassword(forgotPasswordDto: IForgotPassword) {
    return this.httpClient
      .post(this.baseUrl + 'account/forgot-password', forgotPasswordDto);
  }

  getToken() {
    return localStorage.getItem(this.tokenKey) === null
      ? sessionStorage.getItem(this.tokenKey)
      : localStorage.getItem(this.tokenKey);
  }

  getRefreshToken() {
    return localStorage.getItem(this.refreshTokenKey) === null
      ? sessionStorage.getItem(this.refreshTokenKey)
      : localStorage.getItem(this.refreshTokenKey);
  }

  isAuthenticated() {
    return localStorage.getItem(this.tokenKey) !== null
      || sessionStorage.getItem(this.tokenKey) !== null;
  }

  isAuthenticatedAs(userType: string) {
    return this.isAuthenticated() && this.currentUser.userType.toLowerCase() === userType.toLowerCase();
  }

  login(loginType: string, credentials: ICredentials, callback: () => void) {
    const base64Credentials =
      this.convertCredentialsToBase64(credentials.email, credentials.password);

    const requestHeaders = this.headers.append('Authorization', 'Basic ' + base64Credentials);
    const requestUrlSuffix = loginType === LoginTypes.employee
      ? 'employee/token' : 'employer/token';

    return this.httpClient.get<{token: string, refreshToken: string}>(this.baseUrl + requestUrlSuffix, { headers: requestHeaders })
    .subscribe(result => {
      console.log(result);
      if (result === null) {
        return;
      }

      this.decodeJwtDataAndPopulateCurrentUser(result.token);

      if (credentials.rememberMe === true) {
        this.currentUser.rememberMe = true;
        localStorage.setItem(this.tokenKey, result.token);
        localStorage.setItem(this.refreshTokenKey, result.refreshToken);
      } else {
        sessionStorage.setItem(this.tokenKey, result.token);
        sessionStorage.setItem(this.refreshTokenKey, result.refreshToken);
      }
      callback();
    },
    (err) => {
      const errors = err.error;
      for (const key in errors) {
        this.notifyLoginServerError(key, errors[key]);
      }
    });
  }

  logout() {
    localStorage.removeItem(this.tokenKey);
    localStorage.removeItem(this.refreshTokenKey);
    sessionStorage.removeItem(this.tokenKey);
    sessionStorage.removeItem(this.refreshTokenKey);
    this.resetUser();
    this.userLogout$.next();
    this.router.navigate(['/login']);
  }

  notifyLoginServerError(key: string, errorMessages: string[]) {
    this.loginServerErrorOccured$.next({ key: key, errorMessages: errorMessages });
  }

  notifyRegistrationServerError(key: string, errorMessages: string[]) {
    this.registrationServerErrorOccured$.next({ key: key, errorMessages: errorMessages });
  }

  refreshToken() {
    console.log('refresh token');
    return this.httpClient.post<{token: string, refreshToken: string}>(this.baseUrl + 'token/refresh',
      {refreshToken: this.getRefreshToken(), authToken: this.getToken()})
      .pipe(mergeMap((result: {token: string, refreshToken: string}) => {
        console.log(result);
        if (this.currentUser.rememberMe === true) {
          localStorage.setItem(this.tokenKey, result.token);
          localStorage.setItem(this.refreshTokenKey, result.refreshToken);
        } else {
          sessionStorage.setItem(this.tokenKey, result.token);
          sessionStorage.setItem(this.refreshTokenKey, result.refreshToken);
        }

        this.decodeJwtDataAndPopulateCurrentUser(result.token);

        return of([]);
      }));
  }

  registerEmployee(registerEmployeeDto: IRegisterEmployee) {
    return this.httpClient.post(this.baseUrl + 'account/register/employee', registerEmployeeDto);
  }

  registerEmployer(registerEmployerDto: IRegisterEmployer) {
    return this.httpClient.post(this.baseUrl + 'account/register/employer', registerEmployerDto);
  }

  resetPassword(resetPasswordDto: IResetPassword) {
    return this.httpClient
      .post(this.baseUrl + 'account/reset-password', resetPasswordDto);
  }

  resendCoinformationEmail() {
    const requestAuthHeaders = this.headers.append('Authorization', 'Bearer ' + this.getToken());
    return this.httpClient.post(this.baseUrl + 'account/resend-email', null, { headers: requestAuthHeaders });
  }

  // PRIVATE
  private convertCredentialsToBase64(email: string, password: string): string {
    return btoa(email + ':' + password);
  }

  private decodeJwtDataAndPopulateCurrentUser(jwtToken: string) {
    const decodedJwtJsonData = atob(jwtToken.split('.')[1]);
    const decodedJwtData = JSON.parse(decodedJwtJsonData);

    this.currentUser.id = decodedJwtData['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier'];
    this.currentUser.email = decodedJwtData['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress'];
    this.currentUser.userType = decodedJwtData['http://schemas.microsoft.com/ws/2008/06/identity/claims/role'];
  }

  private resetUser() {
    this.currentUser = {
      email: null,
      id: null,
      userType: null,
      rememberMe: false,
    };
  }
}
