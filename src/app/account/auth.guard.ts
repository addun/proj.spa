import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

import { AccountService } from './account.service';


@Injectable()
export class AuthGuard implements CanActivate {
  constructor(public accountService: AccountService, public router: Router) {}
  canActivate(): boolean {
    if (!this.accountService.isAuthenticated()) {
      this.router.navigate(['/login']);
      return false;
    }
    return true;
  }
}