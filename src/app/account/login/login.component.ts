import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { AccountService } from '../account.service';
import { ICredentials } from '../../interfaces/ICredentials';
import { LoginTypes } from '../loginTypes';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  credentials: ICredentials;
  employeeLoginForm: FormGroup;
  employerLoginForm: FormGroup;
  loginInProgress = false;
  serverErrors: string[] = [];
  redirectUrl = null;

  constructor(private accountService: AccountService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.initializeEmployeeForm();
    this.initializeEmployerForm();

    const queryParam = this.activatedRoute.snapshot.queryParams['redirectUrl'];
    if (queryParam) {
      this.redirectUrl = queryParam;
    }

    this.accountService.loginServerErrorOccured$.subscribe(error => {
      this.serverErrors.push(...error.errorMessages);
    });
    console.log(this.redirectUrl);
  }

  changeLoginMode() {
    this.serverErrors.splice(0, this.serverErrors.length);
    this.employeeLoginForm.reset();
    this.employerLoginForm.reset();
  }

  loginAsEmpolyee() {
    this.loginInProgress = true;

    this.serverErrors.splice(0, this.serverErrors.length);
    this.credentials = {
      email:  this.employeeLoginForm.get('email').value,
      password:  this.employeeLoginForm.get('password').value,
      rememberMe:  this.employeeLoginForm.get('rememberMe').value,
    };

    this.accountService.login(LoginTypes.employee, this.credentials, () => {
      this.router.navigateByUrl(this.redirectUrl === null ? 'employee' : this.redirectUrl);
    }).add(() => this.loginInProgress = false);
  }

  loginAsEmpolyer() {
    this.loginInProgress = true;

    this.serverErrors.splice(0, this.serverErrors.length);
    this.credentials = {
      email:  this.employerLoginForm.get('email').value,
      password:  this.employerLoginForm.get('password').value,
      rememberMe:  this.employerLoginForm.get('rememberMe').value,
    };

    this.accountService.login(LoginTypes.employer, this.credentials, () => {
      this.router.navigateByUrl(this.redirectUrl === null ? 'employer' : this.redirectUrl);
    }).add(() => this.loginInProgress = false);
  }

  // PRIVATE
  private initializeEmployeeForm() {
    this.employeeLoginForm = new FormGroup({
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'password': new FormControl(null, [Validators.required]),
      'rememberMe': new FormControl(null),
    });
  }

  private initializeEmployerForm() {
    this.employerLoginForm = new FormGroup({
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'password': new FormControl(null, [Validators.required]),
      'rememberMe': new FormControl(null),
    });
  }
}
