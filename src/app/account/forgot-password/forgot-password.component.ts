import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { AccountService } from '../account.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  forgotPasswordForm: FormGroup;
  resetPasswordEmailSendSuccess: boolean = false;
  confirmEmailInProgress: boolean = false;
  serverErrors: string[] = [];

  constructor(private accountService: AccountService) { }

  ngOnInit() {
    this.registerForm();
  }

  onSubmit(){
    this.confirmEmailInProgress = true;
    
    var forgotPasswordDto = {
      email: this.forgotPasswordForm.get('email').value,
    }

    this.accountService.forgotPassword(forgotPasswordDto)
      .subscribe(() => {
        this.resetPasswordEmailSendSuccess = true;
        this.resetForm();
      },
      error => {
        var errors = error.error;
        for(let key in errors){
          if(this.forgotPasswordForm.get(key) !== null){
            this.forgotPasswordForm.get(key).setErrors({'serverError' : errors[key]});
          }
        }
      }
    ).add(() => this.confirmEmailInProgress = false);
  }

  private registerForm(){
    this.forgotPasswordForm = new FormGroup({
      'email': new FormControl(null, [Validators.required, Validators.email]),
    });
  }

  private resetForm(){
    this.forgotPasswordForm.reset();
  }
}
