import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { AccountService } from '../account.service';
import { RegistrationValidators } from '../../shared/validators/registration.validators';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  resetPasswordForm: FormGroup;
  resetPasswordInProgress = false;
  resetPasswordSuccess = false;

  constructor(private accountService: AccountService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.registerForm();
  }

  onSubmit() {
    this.resetPasswordInProgress = true;

    var resetPasswordDto = {
      userId: this.route.snapshot.params['userId'],
      token: this.route.snapshot.params['token'],
      newPassword: this.resetPasswordForm.get('newPassword').value,
      newPasswordConfirm: this.resetPasswordForm.get('newPasswordConfirm').value,
    }

    this.accountService.resetPassword(resetPasswordDto)
      .subscribe(() => {
        this.resetPasswordSuccess = true;
      },
      error => {
        let errors = error.error;
          for(let key in errors) {
            if (this.resetPasswordForm.get(key) !== null){
              this.resetPasswordForm.get(key).setErrors({'serverError' : errors[key]});
            }
            if (key === '') {
              this.router.navigate(['/error']);
            }
          }
      }).add(() => this.resetPasswordInProgress = false);
  }

  private registerForm() {
    this.resetPasswordForm = new FormGroup({
      'newPassword': new FormControl(null, [Validators.required]),
      'newPasswordConfirm': new FormControl(null,
        [Validators.required, RegistrationValidators.ConfirmEqualValidator('newPassword')]),
    });
  }
}
