import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { AccountService } from '../account.service';

@Component({
  selector: 'app-confirm-email',
  templateUrl: './confirm-email.component.html',
  styleUrls: ['./confirm-email.component.css']
})
export class ConfirmEmailComponent implements OnInit {
  success: boolean = false;  

  constructor(private accountService: AccountService, private route: ActivatedRoute, private router: Router){}
  
  ngOnInit(){
    var confirmEmailDto = {
      userId: this.route.snapshot.params['userId'],
      token: this.route.snapshot.params['token'],
    }

    console.log(confirmEmailDto.token);
    console.log(confirmEmailDto.userId);

    this.accountService.confirmEmail(confirmEmailDto)
    .subscribe(() => {
      this.success = true;
    },
    error => {
      this.router.navigate(['/error']);
    });
  }
}
