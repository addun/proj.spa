import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { Router } from '@angular/router';

import { IRegisterEmployee } from '../../../interfaces/IRegisterEmployee';
import { IRegisterEmployer } from '../../../interfaces/IRegisterEmployer';
import { RegistrationService } from '../registration.service';
import { AccountService } from '../../account.service';

import {polishApplicationGenderOptionsTranslator} from '../../../shared/applicationGenders';


@Component({
  selector: 'app-registration-summary',
  templateUrl: './registration-summary.component.html',
  styleUrls: ['./registration-summary.component.css']
})
export class RegistrationSummaryComponent implements OnInit {
  @HostBinding('hidden')hidden: boolean;
  @Input() stepNumber: number;
  @Input() type = '';

  errorsFromServer: Array<string[]> = new Array<string[]>();
  currentRegistrationType: string;
  employee: IRegisterEmployee;
  employer: IRegisterEmployer;

  polishApplicationGenderOptionsTranslator = polishApplicationGenderOptionsTranslator;

  registrationInProgress = false;

  constructor(
    private registrationService: RegistrationService,
    private accountService: AccountService,
    private router: Router) { }

  ngOnInit() {
    this.registrationService.stepChanged$.subscribe(number => {
      this.stepNumber === number ? this.hidden = false : this.hidden = true;
    });

    this.registrationService.registrationTypeChanged$.subscribe(type => {
      this.currentRegistrationType = type;
      this.errorsFromServer.splice(0, this.errorsFromServer.length);
    });

    if(this.isStep()){
      this.registrationService
        .addRegistrationStep(this.stepNumber, this.type);
    }

    this.employee = this.registrationService.employee;
    this.employer = this.registrationService.employer;
  }

  prev(){
    this.registrationService.prevStep();
  }

  RegisterEmployee() {
    this.clearErrors();
    this.accountService.registerEmployee(this.employee).subscribe(
      (data: any) => {
        console.log(data);
        this.registrationService.registrationComplete();
      },
      (err: any) => {
        console.log(err);
        var errors = err.error;
        for (let key in errors) {
          this.errorsFromServer.push(errors[key]);
          this.accountService
            .notifyRegistrationServerError(key, errors[key]);
        }
      }
    ).add(() => {this.registrationInProgress = false;});
  }

  RegisterEmployer() {
    this.clearErrors();
    this.accountService
      .registerEmployer(this.employer)
      .subscribe((data: any) => {
        console.log(data);
        this.registrationService.registrationComplete();
      },
      (err: any) => {
        console.log(err);
        var errors = err.error;
        for (let key in errors) {
          this.errorsFromServer.push(errors[key]);
          this.accountService
            .notifyRegistrationServerError(key, errors[key]);
        }
      }
    ).add(() => {this.registrationInProgress = false;});;
  }

  submit() {
    if (this.currentRegistrationType === "employee") {
      this.RegisterEmployee();
    } else if (this.currentRegistrationType === "employer") {
      this.RegisterEmployer();
    }

    this.registrationInProgress = true;
  }

  //PRIVATE
  private clearErrors(){
    this.errorsFromServer.splice(0,this.errorsFromServer.length);
  }

  private isStep(){
    return this.stepNumber;
  }
}
