import { Component, OnInit, Input, Output, EventEmitter, HostBinding } from '@angular/core';

import { RegistrationService } from '../registration.service';

@Component({
  selector: 'app-registration-type',
  templateUrl: './registration-type.component.html',
  styleUrls: ['./registration-type.component.css']
})
export class RegistrationTypeComponent implements OnInit {
  @HostBinding('hidden')hidden: boolean;
  @Input() stepNumber: number;
  @Input() type = '';

  constructor(private registrationService: RegistrationService) { }

  ngOnInit() {
    this.registrationService.stepChanged$.subscribe(number => {
      this.stepNumber === number ? this.hidden = false : this.hidden = true;
    });

    if (this.isStep()) {
      this.registrationService
        .addRegistrationStep(this.stepNumber, this.type);
    }
  }

  registerAs(type: string) {
    this.registrationService.setRegistrationType(type);
    this.registrationService.nextStep();
  }

  // PRIVATE
  private isStep() {
    return this.stepNumber;
  }
}
