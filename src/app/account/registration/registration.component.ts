import {Component, OnInit} from "@angular/core";
import { RegistrationService } from "./registration.service";

@Component({
  selector: "app-register",
  templateUrl: "./registration.component.html",
  styleUrls: ["./registration.component.css"],
  providers: [
    RegistrationService
  ]
})
export class RegistrationComponent implements OnInit {
  isRegistrationComplete: boolean = false;

  constructor(private registrationService: RegistrationService) {}

  ngOnInit() {
    this.registrationService.registrationComplete$.subscribe(() => {
      this.isRegistrationComplete = true;
    });
  }
}
