import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IRegisterEmployee } from '../../../interfaces/IRegisterEmployee';
import { RegistrationService } from '../registration.service';
import { AccountService } from '../../account.service';

import {applicationGenderOptions} from '../../../shared/applicationGenders';

@Component({
  selector: 'app-register-personal-data',
  templateUrl: './registration-personal-data.component.html',
  styleUrls: ['./registration-personal-data.component.css']
})
export class RegistrationPersonalDataComponent implements OnInit {
  @HostBinding('hidden')hidden: boolean;
  @Input() stepNumber: number;
  @Input() type = '';

  applicationGenderOptions = applicationGenderOptions;

  employee: IRegisterEmployee;
  personalDataForm: FormGroup;

  constructor(private registrationService: RegistrationService, private accountService: AccountService) { }

  ngOnInit() {
    this.registerForm();

    this.registrationService.stepChanged$.subscribe(number => {
      this.stepNumber === number ? this.hidden = false : this.hidden = true;
    });

    this.registrationService.registrationTypeChanged$.subscribe(type => {
      this.personalDataForm.reset({sex: this.applicationGenderOptions.nodefined});
    });

    this.accountService.registrationServerErrorOccured$.subscribe(error => {
      if(this.personalDataForm.get(error.key) !== null) {
        this.personalDataForm.get(error.key).setErrors({'serverError' : error.errorMessages});
      }
    });

    if (this.isStep()) {
      this.registrationService
        .addRegistrationStep(this.stepNumber, this.type);
    }

    this.employee = this.registrationService.employee;
  }

  prev() {
    this.registrationService.prevStep();
  }

  next() {
    this.assignFormValues();
    this.registrationService.nextStep();
  }

  // PRIVATE
  private assignFormValues() {
    this.employee.about = this.personalDataForm.controls.about.value;
    this.employee.name = this.personalDataForm.controls.name.value;
    this.employee.surname = this.personalDataForm.controls.surname.value;
    this.employee.birthDate = this.personalDataForm.controls.birthDate.value;
    this.employee.sex = this.personalDataForm.controls.sex.value;
    this.employee.phone = this.personalDataForm.controls.phone.value;
    this.employee.country = this.personalDataForm.controls.country.value;
    this.employee.city = this.personalDataForm.controls.city.value;
    this.employee.websiteUrl = this.personalDataForm.controls.websiteUrl.value;
  }

  private isStep() {
    return this.stepNumber;
  }

  private registerForm() {
    this.personalDataForm = new FormGroup({
      'about': new FormControl(null),
      'name': new FormControl(null, Validators.required),
      'surname': new FormControl(null, Validators.required),
      'birthDate': new FormControl(null, Validators.required),
      'sex': new FormControl(this.applicationGenderOptions.nodefined),
      'phone': new FormControl(null),
      'country': new FormControl(null, Validators.required),
      'city': new FormControl(null, Validators.required),
      'websiteUrl': new FormControl(null),
    });
  }
}
