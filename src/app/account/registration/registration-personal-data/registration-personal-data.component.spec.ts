import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrationPersonalDataComponent } from './registration-personal-data.component';

describe('RegisterPersonalDataComponent', () => {
  let component: RegistrationPersonalDataComponent;
  let fixture: ComponentFixture<RegistrationPersonalDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrationPersonalDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationPersonalDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
