import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrationSigninDataComponent } from './registration-signin-data.component';

describe('RegisterSigninDataComponent', () => {
  let component: RegistrationSigninDataComponent;
  let fixture: ComponentFixture<RegistrationSigninDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrationSigninDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationSigninDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
