import { Component, OnInit, Output, EventEmitter, Input, HostBinding } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IRegisterEmployee } from '../../../interfaces/IRegisterEmployee';
import { IRegisterEmployer } from '../../../interfaces/IRegisterEmployer';
import { RegistrationValidators } from '../../../shared/validators/registration.validators';
import { RegistrationService } from '../registration.service';
import { AccountService } from '../../account.service';

@Component({
  selector: 'app-register-signin-data',
  templateUrl: './registration-signin-data.component.html',
  styleUrls: ['./registration-signin-data.component.css']
})
export class RegistrationSigninDataComponent implements OnInit {
  @HostBinding('hidden')hidden: boolean;
  @Input() stepNumber: number;
  @Input() type: string = '';

  employee: IRegisterEmployee;
  employer: IRegisterEmployer;
  signInDataForm: FormGroup;

  constructor(private registrationService: RegistrationService, private accountService: AccountService) { }

  ngOnInit() {
    this.registerForm();
    this.registrationService.stepChanged$.subscribe(number => {
      this.stepNumber === number ? this.hidden = false : this.hidden = true;
    });

    this.registrationService.registrationTypeChanged$.subscribe(type => {
      this.signInDataForm.reset();
    });

    this.accountService.registrationServerErrorOccured$.subscribe(error => {
      if(this.signInDataForm.get(error.key) !== null){
        this.signInDataForm.get(error.key).setErrors({'serverError' : error.errorMessages});
      }
    });

    if(this.isStep()){
      this.registrationService
        .addRegistrationStep(this.stepNumber, this.type);
    }

    this.employee = this.registrationService.employee;
    this.employer = this.registrationService.employer;
  }

  next(){
    this.assignFormValues();
    this.registrationService.nextStep();
  }

  prev(){
    this.registrationService.prevStep();
  }

  //PRIVATE
  private assignFormValues(){
    this.employee.email = this.signInDataForm.controls.email.value;
    this.employee.password = this.signInDataForm.controls.password.value;

    this.employer.email = this.signInDataForm.controls.email.value;
    this.employer.password = this.signInDataForm.controls.password.value;
  }

  private isStep(){
    return this.stepNumber;
  }

  private registerForm(){
    this.signInDataForm = new FormGroup({
      'email': new FormControl(null, {validators: [Validators.required, Validators.email]}),
      'password': new FormControl(null, [Validators.required]),
      'passwordConfirm': new FormControl(null, [Validators.required,
        RegistrationValidators.ConfirmEqualValidator('password')]),
    });
  }
}
