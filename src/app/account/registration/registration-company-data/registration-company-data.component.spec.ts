import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrationCompanyDataComponent } from './registration-company-data.component';

describe('RegisterCompanyDataComponent', () => {
  let component: RegistrationCompanyDataComponent;
  let fixture: ComponentFixture<RegistrationCompanyDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrationCompanyDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationCompanyDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
