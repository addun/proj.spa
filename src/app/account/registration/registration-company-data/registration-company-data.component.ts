import { Component, OnInit, Output, EventEmitter, Input, HostBinding } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IRegisterEmployer } from '../../../interfaces/IRegisterEmployer';
import { RegistrationValidators } from '../../../shared/validators/registration.validators';
import { RegistrationService } from '../registration.service';
import { AccountService } from '../../account.service';

@Component({
  selector: 'app-register-company-data',
  templateUrl: './registration-company-data.component.html',
  styleUrls: ['./registration-company-data.component.css']
})
export class RegistrationCompanyDataComponent implements OnInit {
  @HostBinding('hidden')hidden: boolean;
  @Input() stepNumber: number;
  @Input() type: string = '';

  employer: IRegisterEmployer;
  companyTechnologies: string[] = [];
  technologyInput: string = '';

  companyDataForm: FormGroup;

  constructor(private registrationService: RegistrationService, private accountService: AccountService) { }

  ngOnInit() {
    this.registerForm();

    this.registrationService.stepChanged$.subscribe(number => {
      this.stepNumber === number ? this.hidden = false : this.hidden = true;
    });

    this.registrationService.registrationTypeChanged$.subscribe(type => {
      this.companyDataForm.reset();
    });

    this.accountService.registrationServerErrorOccured$.subscribe(error => {
      if(this.companyDataForm.get(error.key) !== null){
        this.companyDataForm.get(error.key).setErrors({'serverError' : error.errorMessages});
      }
    });

    if(this.isStep()){
      this.registrationService
        .addRegistrationStep(this.stepNumber, this.type);
    }

    this.employer = this.registrationService.employer;
  }

  prev(){
    this.registrationService.prevStep();
  }

  next(){
    this.assignFormValues();
    this.registrationService.nextStep();
  }

  //PRIVATE
  private assignFormValues(){
    this.employer.companyName = this.companyDataForm.controls.companyName.value;
    this.employer.companyDescription = this.companyDataForm.controls.companyDescription.value;
    this.employer.foundedYear = this.companyDataForm.controls.foundedYear.value;
    this.employer.companySize = this.companyDataForm.controls.companySize.value;
    this.employer.nip = this.companyDataForm.controls.nip.value;
    this.employer.websiteUrl = this.companyDataForm.controls.websiteUrl.value;
    this.employer.street = this.companyDataForm.controls.street.value;
    this.employer.apartmentNumber = this.companyDataForm.controls.apartmentNumber.value;
    this.employer.flatNumber = this.companyDataForm.controls.flatNumber.value;
    this.employer.country = this.companyDataForm.controls.country.value;
    this.employer.city = this.companyDataForm.controls.city.value;
    this.employer.postalCode = this.companyDataForm.controls.postalCode.value;
  }

  private isStep(){
    return this.stepNumber;
  }

  private registerForm(){
    this.companyDataForm = new FormGroup({
      'companyName': new FormControl("", Validators.required),
      'companyDescription': new FormControl(""),
      'foundedYear': new FormControl("", [Validators.required,
        Validators.min(1900), Validators.max(new Date(Date.now()).getFullYear())]),
      'companySize': new FormControl("", Validators.required),
      'nip': new FormControl("", [Validators.required, RegistrationValidators.ConfirmNipFormat]),
      'websiteUrl': new FormControl(""),
      'street': new FormControl("", Validators.required),
      'apartmentNumber': new FormControl("", Validators.required),
      'flatNumber': new FormControl(""),
      'country': new FormControl("", Validators.required),
      'city': new FormControl("", Validators.required),
      'postalCode': new FormControl("", Validators.required),
    });
  }
}
