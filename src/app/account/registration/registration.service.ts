import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { IRegisterEmployee } from '../../interfaces/IRegisterEmployee';
import { IRegisterEmployer } from '../../interfaces/IRegisterEmployer';


@Injectable()
export class RegistrationService {
  employee: IRegisterEmployee;
  employer: IRegisterEmployer;

  currentArrayIndex: number = 0;
  currentRegistrationStep: number;
  currentRegistrationType: string = '';
  registrationSteps: Array<{ stepNumber: number, stepType: string }> = [];

  registrationComplete$ : Subject<void> = new Subject();
  registrationTypeChanged$ : Subject<string> = new Subject();
  stepChanged$ : Subject<number> = new Subject();

  constructor() {
    this.initializeRegisterEmployeeDto();
    this.initializeRegisterEmployerDto();
  }

  addRegistrationStep(stepNumber: number, stepType: string){
    if(this.currentArrayIndex !== 0){
      console.log('You should register all steps before you start use them.')
      return;
    }

    this.registrationSteps.push({
      stepNumber: stepNumber,
      stepType: stepType,
    });

    this.registrationSteps.sort((a, b) => {
      return a.stepNumber > b.stepNumber ? 1 : 0;
    });

    this.currentRegistrationStep = this.registrationSteps[this.currentArrayIndex].stepNumber;
    this.stepChanged$.next(this.currentRegistrationStep);
  }

  getRegistrationTypeByStepNumber(stepNumber: number): string{
    var item = this.registrationSteps.find(x => x.stepNumber === stepNumber);
    if(item)
      return item.stepType;

    return null;
  }

  nextStep(){
    this.findNextStep();
    this.stepChanged$.next(this.currentRegistrationStep);
  }

  prevStep(){
    this.findPrevStep();
    this.stepChanged$.next(this.currentRegistrationStep);
  }

  registrationComplete(){
    this.registrationComplete$.next();
  }

  setRegistrationType(type: string){
    this.currentRegistrationType = type;
    this.registrationTypeChanged$.next(this.currentRegistrationType);
  }

  // PRIVATE
  private findNextStep() {
    for (var i = this.currentArrayIndex + 1; i < this.registrationSteps.length; i++) {
      if (this.registrationSteps[i].stepType === '' || this.registrationSteps[i].stepType === this.currentRegistrationType) {
        this.currentRegistrationStep = this.registrationSteps[i].stepNumber;
        this.currentArrayIndex = i;
        break;
      }
    }
  }

  private findPrevStep(){
    for (var i = this.currentArrayIndex - 1; i >= 0; i--) {
      if (this.registrationSteps[i].stepType === '' || this.registrationSteps[i].stepType === this.currentRegistrationType) {
        this.currentRegistrationStep = this.registrationSteps[i].stepNumber;
        this.currentArrayIndex = i;
        break;
      }
    }
  }

  private getCurrentRegistrationStep(): number {
    return this.registrationSteps[this.currentArrayIndex].stepNumber;
  }

  private initializeRegisterEmployeeDto(){
    this.employee = {
      about: '',
      name: '',
      surname: '',
      email: '',
      password: '',
      birthDate: '',
      city: '',
      country: '',
      phone: '',
      sex: '',
      websiteUrl: '',
    };
  }

  private initializeRegisterEmployerDto(){
    this.employer = {
      email: "",
      password: "",
      companyName: "",
      nip: "",
      companyDescription: "",
      companySize: "",
      foundedYear: "",
      websiteUrl: "",
      street: "",
      apartmentNumber: "",
      flatNumber: "",
      country: "",
      city: "",
      postalCode: ""
    };
  }
}
