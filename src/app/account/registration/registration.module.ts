import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';

import { HeaderStepDirective } from './header-step/header-step.directive';
import { RegistrationCompanyDataComponent } from './registration-company-data/registration-company-data.component';
import { RegistrationComponent } from './registration.component';
import { RegistrationPersonalDataComponent } from './registration-personal-data/registration-personal-data.component';
import { RegistrationSigninDataComponent } from './registration-signin-data/registration-signin-data.component';
import { RegistrationSummaryComponent } from './registration-summary/registration-summary.component';
import { RegistrationTypeComponent } from './registration-type/registration-type.component';



@NgModule({
  declarations: [
    HeaderStepDirective,
    RegistrationCompanyDataComponent,
    RegistrationComponent,
    RegistrationPersonalDataComponent,
    RegistrationSigninDataComponent,
    RegistrationSummaryComponent,
    RegistrationTypeComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule,
  ],
})
export class RegistrationModule { }
