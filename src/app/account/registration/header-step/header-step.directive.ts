import { Directive, OnInit, HostBinding, HostListener, Input, Output, EventEmitter } from '@angular/core';
import { RegistrationService } from '../registration.service';

@Directive({
  selector: '[head-reg-step]'
})
export class HeaderStepDirective {
  @HostBinding('class.active')activeClass: boolean;
  @HostBinding('class.disabled')disabledClass: boolean = true;
  @HostBinding('class.done')doneClass: boolean;

  @Input() order: number;

  constructor(private registrationService: RegistrationService) {}

  ngOnInit(){
    this.registrationService.stepChanged$.subscribe(stepNumber => {
      if(stepNumber === this.order)
        this.activeClass = true;
      else
        this.activeClass = false;

      if(stepNumber > this.order && this.disabledClass === false)
        this.doneClass = true;
      else
        this.doneClass = false;
    });

    this.registrationService.registrationTypeChanged$.subscribe(newType => {
      var type = this.registrationService.getRegistrationTypeByStepNumber(this.order);
      if(newType !== type && type !== '')
        this.disabledClass = true;
      else
        this.disabledClass = false;
    });
  }
}
