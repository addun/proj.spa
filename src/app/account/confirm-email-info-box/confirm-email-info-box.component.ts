import { Component, OnInit } from '@angular/core';

import { AccountService } from '../account.service';

@Component({
  selector: 'app-confirm-email-info-box',
  templateUrl: './confirm-email-info-box.component.html',
  styleUrls: ['./confirm-email-info-box.component.css']
})
export class ConfirmEmailInfoBoxComponent implements OnInit {
  resendingEmailInProgress: boolean = false;

  constructor(private accountService: AccountService) { }

  ngOnInit() {
  }

  sendEmailAgain($event: Event) {
    $event.preventDefault();
    this.resendingEmailInProgress = true;
    this.accountService.resendCoinformationEmail()
      .subscribe(data => {},
      err => {
        console.log(err);
      }
    ).add(() => { this.resendingEmailInProgress = false; });
  }

}
