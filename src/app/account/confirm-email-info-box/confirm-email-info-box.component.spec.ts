import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmEmailInfoBoxComponent } from './confirm-email-info-box.component';

describe('ConfirmEmailInfoBoxComponent', () => {
  let component: ConfirmEmailInfoBoxComponent;
  let fixture: ComponentFixture<ConfirmEmailInfoBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmEmailInfoBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmEmailInfoBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
