export interface IEmployerData {
  email: string;
  companyName: string;
  companyDescription: string;
  companySize: string;
  foundedYear: string;
  nip: string;
  websiteUrl: string;
}
