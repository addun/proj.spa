export interface IEmployeeCertificate {
  courseId: string;
  courseName: string;
  monthOfObtain: string;
  yearOfObtain: string;
  organizerName: string;
}
