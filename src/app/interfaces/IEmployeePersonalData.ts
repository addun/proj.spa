export interface IEmployeePersonalData {
  email: string;
  name: string;
  surname: string;
  about: string;
  birthDate: string;
  sex: string;
  phone: string;
  country: string;
  city: string;
  websiteUrl: string;
}
