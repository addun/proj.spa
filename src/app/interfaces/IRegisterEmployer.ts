export interface IRegisterEmployer {
    email: string;
    password: string;
    companyName: string;
    companyDescription: string;
    foundedYear: string;
    companySize: string;
    nip: string;
    websiteUrl: string;
    street: string;
    apartmentNumber: string;
    flatNumber: string;
    country: string;
    city: string;
    postalCode: string;
}
