export interface IEmployeeJobExperience {
  professionalExperienceId: string;
  position: string;
  companyName: string;
  startMonth: string;
  startYear: string;
  endMonth: string;
  endYear: string;
  country: string;
  city: string;
  jobDescription: string;
}
