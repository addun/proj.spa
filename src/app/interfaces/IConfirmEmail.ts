export interface IConfirmEmailDto {
  userId: string;
  token: string;
}
