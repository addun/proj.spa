export interface IResetPassword {
    userId: string;
    token: string;
    newPassword: string;
    newPasswordConfirm: string;
}
