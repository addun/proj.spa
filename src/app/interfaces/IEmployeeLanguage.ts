export interface IEmployeeLanguage {
  languageId: string;
  languageName: string;
  level: number;
}
