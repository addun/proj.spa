export interface IRegisterEmployee {
    about: string;
    email: string;
    password: string;
    name: string;
    surname: string;
    birthDate: string;
    sex: string;
    phone: string;
    country: string;
    city: string;
    websiteUrl: string;
}
