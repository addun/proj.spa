export interface IApplicant {
  id: string;
  name: string;
  surname: string;
  email: string;
  phone: string;
}

