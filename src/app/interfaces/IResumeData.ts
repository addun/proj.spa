export interface IResumeData {
  fileName: string;
  contentType: string;
  contentTypeShort: string;
  content: string;
}
