export interface ICredentials {
    email: string;
    password: string;
    rememberMe: boolean;
}
