export interface IEmployeeEducation {
  educationId: string;
  schoolName: string;
  startYear: string;
  endYear: string;
  branchOfStudy: string;
  speciality: string;
  degree: string;
}
