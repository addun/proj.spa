export interface IEditEmployeeLanguage {
  languageId: string;
  level: number;
}
