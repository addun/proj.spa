import {IEmployeeCertificate} from './IEmployeeCertificate';
import {IEmployeeEducation} from './IEmployeeEducation';
import {IEmployeeLanguage} from './IEmployeeLanguage';
import {IEmployeeSkill} from './IEmployeeSkill';
import {IEmployeeJobExperience} from './IEmployeeJobExperience';
import {IEmployeeJobOffer} from './IEmployeeJobOffer';

export interface IEmployee {
    email: string;
    emailConfirmed: boolean;
    name: string;
    surname: string;
    about: string;
    birthDate: string;
    sex: string;
    phone: string;
    country: string;
    city: string;
    websiteUrl: string;
    employeeCertificates: IEmployeeCertificate[];
    employeeEducation: IEmployeeEducation[];
    employeeLanguages: IEmployeeLanguage[];
    employeeSkills: IEmployeeSkill[];
    jobExperience: IEmployeeJobExperience[];
    applications: IEmployeeJobOffer[];
    favourites: IEmployeeJobOffer[];
}
