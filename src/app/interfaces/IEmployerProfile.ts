import {IEmployerLocation} from './IEmployerLocation';
import {IEmployerBenefit} from './IEmployerBenefit';
import {IEmployerTechnology} from './IEmployerTechnology';
import {IEmployerJobOfferForDisplay} from './IEmployerJobOfferForDisplay';

export interface IEmployerProfile {
  email: string;
  emailConfirmed: boolean;
  companyName: string;
  companyDescription: string;
  companySize: string;
  foundedYear: string;
  nip: string;
  websiteUrl: string;
  whatsWeCreatingDescription: string;
  workingWithUsDescription: string;
  internshipDescription: string;
  headquartersId: string;
  locations: IEmployerLocation[];
  benefits: IEmployerBenefit[];
  jobOffers: IEmployerJobOfferForDisplay[];
  technologies: IEmployerTechnology[];
}
