export interface IEmployerBenefit {
  benefitId: string;
  title: string;
  description: string;
}

