export interface IEditEmployeeSkill {
  skillId: string;
  level: number;
}
