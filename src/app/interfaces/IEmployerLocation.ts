export interface IEmployerLocation {
  employerLocationId: string;
  country: string;
  city: string;
  street: string;
  apartmentNumber: string;
  flatNumber: string;
  postalCode: string;
}

