import {IJobOfferEmployer} from './IJobOfferEmployer';

export interface IEmployeeJobOffer {
  offerId: string;
  title: string;
  country: string;
  city: string;
  address: string;
  creationDate: string;
  salaryFrom: string;
  salaryTo: string;
  currency: string;
  workTime: string;
  invoiceType: string;
  experienceLevel: string;
  offerDescription: string;
  remote: string;
  employer: IJobOfferEmployer;
  skills: string[];
}
