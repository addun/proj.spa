export interface IEmployerTechnology {
  technologyId: string;
  technologyName: string;
}
