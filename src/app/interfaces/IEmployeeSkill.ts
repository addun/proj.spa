export interface IEmployeeSkill {
  skillId: string;
  skillName: string;
  level: number;
}
