import {IEmployerLocation} from './IEmployerLocation';
import {IEmployerBenefit} from './IEmployerBenefit';
import {IEmployerJobOffer} from './IEmployerJobOffer';
import {IEmployerTechnology} from './IEmployerTechnology';

export interface IEmployer {
    email: string;
    emailConfirmed: boolean;
    companyName: string;
    companyDescription: string;
    companySize: string;
    foundedYear: string;
    nip: string;
    websiteUrl: string;
    whatsWeCreatingDescription: string;
    workingWithUsDescription: string;
    internshipDescription: string;
    headquartersId: string;
    locations: IEmployerLocation[];
    benefits: IEmployerBenefit[];
    jobOffers: IEmployerJobOffer[];
    technologies: IEmployerTechnology[];
}
