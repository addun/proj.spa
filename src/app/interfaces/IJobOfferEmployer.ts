export interface IJobOfferEmployer {
  employerId: string;
  companyName: string;
  companyDescription: string;
  companySize: string;
  foundedYear: string;
}
