import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AccountModule } from './account/account.module';
import { EmployeeModule } from './employee/employee.module';
import { RegistrationModule } from './account/registration/registration.module';
import { SharedModule } from './shared/shared.module';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { NotFoundComponent } from './not-found/not-found.component';

import { AccountService } from './account/account.service';
import { AuthGuard } from './account/auth.guard';
import {AutocompleteService} from './shared/services/autocomplete.service';
import {EmployerModule} from './employer/employer.module';
import {AuthErrorInterceptor} from './shared/services/auth-error-interceptor';
import {EmployeeService} from './employee/employee.service';
import {EmployerService} from './employer/employer.service';
import { IndexComponent } from './index/index.component';
import {SearchModule} from './search/search.module';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    NotFoundComponent,
    IndexComponent,
  ],
  imports: [
    AppRoutingModule,
    AccountModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    RegistrationModule,
    SharedModule,
    EmployeeModule,
    EmployerModule,
    SearchModule
  ],
  providers: [
    AccountService,
    AuthGuard,
    AutocompleteService,
    EmployeeService,
    EmployerService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthErrorInterceptor,
      multi: true
    }
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
