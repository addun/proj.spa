import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrowseJobOfferComponent } from './browse-job-offer.component';

describe('BrowseJobOfferComponent', () => {
  let component: BrowseJobOfferComponent;
  let fixture: ComponentFixture<BrowseJobOfferComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrowseJobOfferComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrowseJobOfferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
