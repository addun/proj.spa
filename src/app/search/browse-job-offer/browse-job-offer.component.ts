import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {IEmployerJobOffer} from '../../interfaces/IEmployerJobOffer';
import {SearchService} from '../search.service';
import {Subscription} from 'rxjs';
import {AccountService} from '../../account/account.service';
import {LoginTypes} from '../../account/loginTypes';
import {EmployeeService} from '../../employee/employee.service';
import {IEmployee} from '../../interfaces/IEmployee';
import {IEmployeeJobOffer} from '../../interfaces/IEmployeeJobOffer';

@Component({
  selector: 'app-browse-job-offer',
  templateUrl: './browse-job-offer.component.html',
  styleUrls: ['./browse-job-offer.component.css']
})
export class BrowseJobOfferComponent implements OnInit, OnDestroy {
  @ViewChild('fileInput') fileInput: ElementRef;

  applyForJobInProgress = false;

  userApplications: IEmployeeJobOffer[];
  wasAppliedBefore = false;

  favourites: IEmployeeJobOffer[];
  isSavedAsFavourite = false;

  jobOffer: IEmployerJobOffer;
  selectedFile: File = null;

  // indicate if user watching this is employee
  authenticatedAsEmployee = false;

  // hold value of query param used during searching for job
  searchUrl: null;

  jobOfferSubscription: Subscription;
  employeeSubscription: Subscription;

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private searchService: SearchService,
              private accountService: AccountService, private employeeService: EmployeeService) { }

  ngOnInit() {
    this.authenticatedAsEmployee = this.accountService.isAuthenticatedAs('employee')
    this.activatedRoute.params.subscribe(params => {
      const id = params['id'];

      if (this.authenticatedAsEmployee) {
        this.employeeSubscription = this.employeeService.getCurrentEmployee()
          .subscribe((emp: IEmployee) => {
            this.userApplications = emp.applications;
            this.favourites = emp.favourites;
            this.wasAppliedBefore = this.userApplyBefore(id);
            this.isSavedAsFavourite = this.isMarkAsFavourite(id);
            console.log(this.wasAppliedBefore);
            console.log(this.isSavedAsFavourite);
          });
      }

      const queryParam =  this.activatedRoute.snapshot.queryParams['searchurl'];
      console.log(queryParam);
      if (queryParam) {
        this.searchUrl = queryParam;
      }

      this.jobOfferSubscription = this.searchService.getJobOffer(id)
        .subscribe((data: IEmployerJobOffer) => {
          console.log(data);
          this.jobOffer = data;
          console.log(this.jobOffer);
        },
          () => {
          this.router.navigate(['/not-found']);
        });
    });
  }

  applyForJob() {
    this.applyForJobInProgress = true;
    this.employeeService.applyForJob(this.jobOffer.offerId, this.selectedFile)
      .subscribe(() => {
        this.router.navigate(['/employee/jobs']);
      })
      .add(() => this.applyForJobInProgress = false);

    this.resetFileInput();
  }

  deleteFromFavourite() {
    this.employeeService.deleteJobOfferFromFavourites(this.jobOffer.offerId)
      .subscribe(() => {});
  }

  markAsFavourite() {
    this.employeeService.addJobOfferToFavourites(this.jobOffer.offerId)
      .subscribe(() => {});
  }

  onBackButtonClick() {
    this.router.navigateByUrl(this.searchUrl);
  }

  onFileChanged($event) {
    this.selectedFile = (<any>$event.target).files[0];
    console.log(this.selectedFile);
  }

  redirectToLoginPage() {
    this.router.navigate(['/login'], { queryParams: { redirectUrl: this.router.url } });
  }

  ngOnDestroy(): void {
    this.jobOfferSubscription.unsubscribe();

    if (this.employeeSubscription) {
      this.employeeSubscription.unsubscribe();
    }
  }

  private resetFileInput() {
    this.fileInput.nativeElement.value = '';
    this.selectedFile = null;
  }

  private isMarkAsFavourite(jobId: string): boolean {
    return this.favourites.findIndex(el => el.offerId === jobId) !== -1;
  }

  private userApplyBefore(jobId: string): boolean {
    return this.userApplications.findIndex(el => el.offerId === jobId) !== -1;
  }
}
