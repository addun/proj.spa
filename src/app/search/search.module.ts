import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {SearchComponent} from './search.component';
import {SearchResultsComponent} from './search-results/search-results.component';
import {EmployeeJobOfferTabComponent} from '../shared/components/employee-job-offer-tab/employee-job-offer-tab.component';
import {BrowseJobOfferComponent} from './browse-job-offer/browse-job-offer.component';
import {FormsModule} from '@angular/forms';
import {SearchService} from './search.service';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule
  ],
  declarations: [
    SearchComponent,
    SearchResultsComponent,
    BrowseJobOfferComponent
  ],
  exports: [
    SearchComponent
  ],
  providers: [SearchService]
})
export class SearchModule { }
