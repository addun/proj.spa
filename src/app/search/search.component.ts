import {Component, OnInit, ViewChild, ElementRef, Input, EventEmitter, Output} from '@angular/core';
import { NgForm } from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  @ViewChild('f') form: NgForm;
  @ViewChild('searchInput') phraseInput: ElementRef;

  @Input() size = 'normal';
  @Input() skillsInit: Array<string> = [];
  @Input() citiesInit: Array<string> = [];
  @Input() salaryInit = '';

  @Output() searchClick = new EventEmitter<{skillTags: string[], cityTags: string[], salaryTag: string}>();

  title = 'justfindit';
  skillTags: string[] = [];
  cityTags: string[] = [];
  salaryTag = '';
  phrase = '';

  constructor(private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    if (this.skillsInit) {
      for (let i = 0; i < this.skillsInit.length; i++) {
        this.phrase = this.skillsInit[i];
        if (!this.isPhraseEmpty()) {
          this.addToTagsArray();
        }
      }
    }

    if (this.citiesInit) {
      for (let i = 0; i < this.citiesInit.length; i++) {
        this.phrase = '[' + this.citiesInit[i] + ']';
        if (!this.isPhraseEmpty()) {
          this.addToTagsArray();
        }
      }
    }

    if (this.salaryInit) {
      this.phrase = this.salaryInit;
      if (!this.isPhraseEmpty()) {
        this.addToTagsArray();
      }
    }
  }

  addToTagsArray() {
    this.phrase = this.phrase.trim();

    if (this.phrase.startsWith('>')) {
      this.salaryTag = this.phrase;
    } else if (this.phrase.startsWith('[') && this.phrase.endsWith(']')) {
      this.cityTags.push(this.phrase.toLowerCase().substring(1, this.phrase.length - 1));
    } else {
      this.skillTags.push(this.phrase);
    }
    this.phrase = '';
  }

  isPhraseEmpty() {
    return this.phrase === null || this.phrase.trim() === '' ? true : false;
  }

  onCityTagRemove(i: number) {
    this.cityTags.splice(i, 1);
  }

  // MOBILE PURPOSES
  onInput($event: KeyboardEvent) {
    if ($event.keyCode === 229) {
      if (!this.isPhraseEmpty() && this.phrase.endsWith('  ')) {
        this.addToTagsArray();
      }
    }
  }

  onKeyDown($event: KeyboardEvent) {
    // TAB
    if ($event.keyCode === 9) {
      $event.preventDefault();

      if (this.isPhraseEmpty()) {
        return;
      }

      this.addToTagsArray();
    } else if ($event.keyCode === 32) {
      // SPACE
      if (!this.isPhraseEmpty() && this.phrase[this.phrase.length - 1] === ' ') {
        $event.preventDefault();
        this.addToTagsArray();
      }
    } else if ($event.keyCode === 13) {
      // ENTER
      if (this.isPhraseEmpty()) {
        this.onSubmit();
      } else {
        $event.preventDefault();
        this.addToTagsArray();
      }
    } else if ($event.keyCode === 8) {
      // BACKSPACE
      if (this.phrase.length === 0) {
        if (this.skillTags.length > 0) {
          this.onSkillTagRemove(this.skillTags.length - 1);
        } else if (this.cityTags.length > 0) {
          this.onCityTagRemove(this.skillTags.length - 1);
        } else {
          this.onSalaryTagRemove();
        }
      }
    }
  }

  onSalaryTagRemove() {
    this.salaryTag = '';
  }

  onSearchboxClick() {
    this.phraseInput.nativeElement.focus();
  }

  onSkillTagRemove(i: number) {
    this.skillTags.splice(i, 1);
  }

  onSubmit() {
    if (!this.isPhraseEmpty()) {
      this.addToTagsArray();
    }

    if (this.skillTags.length <= 0 && this.cityTags.length <= 0 && this.salaryTag === '') {
      return;
    }

    this.searchClick.emit({skillTags: this.skillTags, cityTags: this.cityTags, salaryTag: this.salaryTag});

    // this.resetForm();
  }

  resetForm() {
    this.form.reset();
    this.skillTags.splice(0, this.skillTags.length);
    this.cityTags.splice(0, this.cityTags.length);
    this.salaryTag = '';
    this.phrase = '';
  }
}
