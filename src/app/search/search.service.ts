import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable()
export class SearchService {
  private baseUrl = 'http://localhost:5000/api/';
  // private baseUrl = 'https://justfindit-api.azurewebsites.net/api/';
  private headers: HttpHeaders;
  // private currentEmployer$: ReplaySubject<IEmployer> = new ReplaySubject<IEmployer>(1);

  constructor(private httpClient: HttpClient) {
    this.headers = new HttpHeaders();
  }

  public searchJobOffers(skills: string[], cities: string[], salary: string, currency: string) {
    return this.httpClient
      .get(this.baseUrl + 'job-search', {params:
          {skills: skills, cities: cities, salary: salary, currency: currency}
      });
  }

  public getJobOffer(offerId: string) {
    return this.httpClient
      .get(this.baseUrl + 'job/' + offerId);
  }
}
