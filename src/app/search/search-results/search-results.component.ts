import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {SearchService} from '../search.service';
import {applicationCurrencies} from '../../shared/applicationCurrencies';
import {IEmployerJobOffer} from '../../interfaces/IEmployerJobOffer';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.css']
})
export class SearchResultsComponent implements OnInit, OnDestroy {
  jobOffers: IEmployerJobOffer[] = [];

  skills: Array<string> = new Array<string>();
  cities: Array<string> = new Array<string>();
  salary = '';

  salaryNumber = '';
  currency = '';

  applicationCurrencies = applicationCurrencies;

  searchServiceSubscription: Subscription;

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private searchService: SearchService) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      if (!params.skills && !params.cities && !params.salary) {
        return;
      }

      this.resetValues();

      if (params.skills) {
        if (Array.isArray(params.skills)) {
          for (let i = 0; i < params.skills.length; i++) {
            this.skills.push(params.skills[i]);
          }
        } else {
          this.skills.push(params.skills);
        }
      }

      if (params.cities) {
        if (Array.isArray(params.cities)) {
          for (let i = 0; i < params.cities.length; i++) {
            this.cities.push(params.cities[i]);
          }
        } else {
          this.cities.push(params.cities);
        }
      }

      if (params.salary && !Array.isArray(params.salary)) {
        this.salary = params.salary;

        const currencies = [];
        for (const prop in this.applicationCurrencies) {
          currencies.push(applicationCurrencies[prop]);
        }
        const found = currencies.some((val) => this.salary.indexOf(val) >= 0);
        if (found) {
          this.salaryNumber = this.salary.slice(1, this.salary.length - 3);
          this.currency = this.salary.slice(this.salary.length - 3);
        }
      }

      this.searchServiceSubscription = this.searchService.searchJobOffers(this.skills, this.cities, this.salaryNumber, this.currency)
        .subscribe((data: IEmployerJobOffer[]) => {
          console.log(data);
          this.jobOffers = data;
          console.log(this.jobOffers);
        });
    });
  }


  onSearchClick(tags: { skillTags: string[]; cityTags: string[]; salaryTag: string }) {
    const params = {
      skills: tags.skillTags.length > 0 ? tags.skillTags : null,
      cities: tags.cityTags.length > 0 ? tags.cityTags : null,
      salary: tags.salaryTag !== '' ? tags.salaryTag : null,
    };

    this.router.navigate(['/search'], {queryParams: params});
  }

  ngOnDestroy(): void {
    if (this.searchServiceSubscription === null) {
      this.searchServiceSubscription.unsubscribe();
    }
  }

  private resetValues() {
    this.skills = new Array<string>();
    this.cities = new Array<string>();
    this.salary = '';
    this.salaryNumber = '';
    this.currency = '';
  }
}
