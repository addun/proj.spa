import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ConfirmEmailComponent } from './account/confirm-email/confirm-email.component';
import { EmployeeComponent } from './employee/employee-profile/employee.component';
import { EmployerComponent } from './employer/employer-profile/employer.component';
import { EmployerJobOffersComponent } from './employer/employer-job-offers/employer-job-offers.component';
import { ForgotPasswordComponent } from './account/forgot-password/forgot-password.component';
import { LoginComponent } from './account/login/login.component';
import { RegistrationComponent } from './account/registration/registration.component';
import { ResetPasswordComponent } from './account/reset-password/reset-password.component';

import { AuthGuard } from './account/auth.guard';
import { NotFoundComponent } from './not-found/not-found.component';
import { ErrorComponent } from './shared/components/error/error.component';
import {AddJobOfferComponent} from './employer/employer-job-offers/add-job-offer/add-job-offer.component';
import {JobDetailsComponent} from './employer/employer-job-offers/job-details/job-details.component';
import {EditJobOfferComponent} from './employer/employer-job-offers/edit-job-offer/edit-job-offer.component';
import {BrowseJobOfferComponent} from './search/browse-job-offer/browse-job-offer.component';
import {SearchResultsComponent} from './search/search-results/search-results.component';
import {IndexComponent} from './index/index.component';
import {EmployeePublicProfileComponent} from './employee/employee-public-profile/employee-public-profile.component';
import {EmployerPublicProfileComponent} from './employer/employer-public-profile/employer-public-profile.component';
import {EmployeeJobsComponent} from './employee/employee-jobs/employee-jobs.component';
import {EmployeeFavouriteJobsComponent} from './employee/employee-favourite-jobs/employee-favourite-jobs.component';

const appRoutes: Routes = [
    { path: '', component: IndexComponent },
    { path: 'register', component: RegistrationComponent },
    { path: 'login', component: LoginComponent },
    { path: 'search', component: SearchResultsComponent },
    { path: 'job-offer/:id', component: BrowseJobOfferComponent },
    { path: 'applicant/:id', component: EmployeePublicProfileComponent },
    { path: 'company/:id', component: EmployerPublicProfileComponent },
    { path: 'confirm-email/:userId/:token', component: ConfirmEmailComponent },
    { path: 'reset-password/:userId/:token', component: ResetPasswordComponent },
    { path: 'forgot-password', component: ForgotPasswordComponent },
    { path: 'employee', component: EmployeeComponent, canActivate: [AuthGuard] },
    { path: 'employee/jobs', component: EmployeeJobsComponent, canActivate: [AuthGuard] },
    { path: 'employee/favourites', component: EmployeeFavouriteJobsComponent, canActivate: [AuthGuard] },
    { path: 'employer', component: EmployerComponent, canActivate: [AuthGuard] },
    { path: 'employer/job-offers', component: EmployerJobOffersComponent, canActivate: [AuthGuard] },
    { path: 'employer/job-offer/:id', component: JobDetailsComponent, canActivate: [AuthGuard] },
    { path: 'employer/job-offers/new', component: AddJobOfferComponent, canActivate: [AuthGuard] },
    { path: 'employer/job-offers/edit/:id', component: EditJobOfferComponent, canActivate: [AuthGuard] },
    { path: 'error', component: ErrorComponent },
    { path: 'not-found', component: NotFoundComponent },
    { path: '**', component: NotFoundComponent },
  ];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes, {useHash: true})],
    exports: [RouterModule]
})
export class AppRoutingModule { }
