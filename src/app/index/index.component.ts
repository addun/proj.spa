import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {
  constructor(private router: Router) { }

  ngOnInit() {
  }

  onSearchClick(tags: { skillTags: string[]; cityTags: string[]; salaryTag: string }) {
    const params = {
      skills: tags.skillTags.length > 0 ? tags.skillTags : null,
      cities: tags.cityTags.length > 0 ? tags.cityTags : null,
      salary: tags.salaryTag !== '' ? tags.salaryTag : null,
    };

    this.router.navigate(['/search'], {queryParams: params});
  }
}
