import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AccountService } from '../account/account.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  constructor(private accountService: AccountService, private router: Router) { }

  ngOnInit() {
  }

  isAuthenticated() {
    return this.accountService.isAuthenticated();
  }

  isAuthenticatedAs(userType: string) {
    return this.accountService.isAuthenticatedAs(userType);
  }

  onLogout() {
    this.accountService.logout();
  }
}
