import os, sys, shutil
from git import Repo
from distutils.dir_util import copy_tree

if(len(sys.argv) != 3):
    print("Incorrect number of arguments.")
    sys.exit()

fromDirectory = sys.argv[1]
destDirectory = sys.argv[2]

if(not os.path.exists(fromDirectory) or not os.path.exists(destDirectory)):
    print("One both paths doesn't exist")
    sys.exit()

commitMessage = ''
print("Deleting current deployed project files...")
for fname in os.listdir(destDirectory):
    path = os.path.join(destDirectory, fname)

    if os.path.isdir(path):
        if(fname == '.git'):
            continue
        
        shutil.rmtree(path)
    else:
        os.remove(path)

print("Starting copy new files...")
fromRepo = Repo('.')
commitMessage = fromRepo.head.commit.message.strip()

copy_tree(fromDirectory, destDirectory)
print("Starting deploy...")

destRepo = Repo(destDirectory)

destRepo.git.execute('git add -A')

lastCommitMessage = destRepo.head.commit.message.strip()

file = open('commitNumber.txt','r')
number = file.readline().rstrip()
repeatedCommit = file.readline().rstrip()
file.close()

if(lastCommitMessage == commitMessage or (repeatedCommit != '' and repeatedCommit == commitMessage)):
    oldCommitMessage = commitMessage
    commitMessage = commitMessage + "(version: " + number + ")"
    file = open('commitNumber.txt','w')
    file.write(str(int(number) + 1) + '\n')
    file.write(oldCommitMessage)
    file.close()
else:
    file = open('commitNumber.txt','w')
    file.write('2')
    file.close()

destRepo.index.commit(commitMessage)

origin = destRepo.remote('origin')
origin.push()

print("Finished.")